<?php
/***
*
* Copyright (c) 2011, Yves BOURVON. <groovyprog AT gmail DOT com>
* All rights reserved.
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the names of Yves Bourvon, Groovyprog, phpBackup4sql nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS" AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
***/

require_once ("./config/config.inc.php");
require 'phpBackup4mysql.class.php';

// Initialize source and destination datable through variable
$source_db_name="gestiondeinventario";
$retore_to_db_name="backup_gestiondeinventario_080415-1.sql";


//List the availablble backups as links from "$source_db_name" backups
foreach (glob(BACKUP_DIRECTORY."/".$source_db_name."/*") as $val)
{
echo "<a href='testRestore.php?sql=".str_replace(BACKUP_DIRECTORY.'/'.$source_db_name.'/','',$val)."'>".$val."</a><br/>";
}
if (isset($_GET['sql']))
{

//Create a new phpbackup4mysql instance
$pb4ms = new phpBackup4mysql();

//Make a new db connexion
//and restore db to new db "$retore_to_db_name"
$dbh = $pb4ms->dbconnect();
$sql_dump = $pb4ms->restoreSQL(BACKUP_DIRECTORY.'/'.$source_db_name.'/'.$_GET['sql'],array($retore_to_db_name,$dbh));
if ($sql_dump)
echo json_encode("true");
else
echo json_encode("false");

}

?>