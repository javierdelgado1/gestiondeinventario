-- SQL Dump
-- Exported with phpBackup4MySQL
-- http://www.groovyprog.com
--
-- Php version: 5.5.8 / MySQL version: 5.6.15-log
-- Date: 09-Apr-2015
--
-- Database: `gestiondeinventario`

SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


--
-- Table 'accion' creation.
--

DROP TABLE IF EXISTS `accion`;

CREATE TABLE IF NOT EXISTS `accion` (
  `idaccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idaccion`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


--
-- Dump data of `accion` 
--

INSERT INTO `accion` ( `idaccion`, `nombre` ) VALUES 
(5, 'Almacenamiento'),
(3, 'Asignacion'),
(6, 'Crear'),
(4, 'Desincorporar'),
(1, 'Mantenimiento Preventivo'),
(2, 'Reconfiguracion');



-- -------------------------------------------------------------------------------------------



--
-- Table 'area' creation.
--

DROP TABLE IF EXISTS `area`;

CREATE TABLE IF NOT EXISTS `area` (
  `idareas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`idareas`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'cargo' creation.
--

DROP TABLE IF EXISTS `cargo`;

CREATE TABLE IF NOT EXISTS `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cargo` (`cargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'equipo' creation.
--

DROP TABLE IF EXISTS `equipo`;

CREATE TABLE IF NOT EXISTS `equipo` (
  `idtipo` varchar(50) NOT NULL,
  `idequipo` int(11) NOT NULL AUTO_INCREMENT,
  `idaccion` int(11) NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idmodelo` int(11) NOT NULL,
  `idespecificacion` int(11) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idmotivo` int(11) NOT NULL,
  `idasignadopor` int(11) NOT NULL,
  `idresponsablenuevo` int(11) NOT NULL,
  `personal` int(11) NOT NULL,
  `idarea` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fechacompra` date NOT NULL,
  `fechaasignacion` date NOT NULL,
  `fecharealizado` date NOT NULL,
  `codigoexterno` varchar(50) NOT NULL,
  `numerofactura` varchar(3) NOT NULL,
  `piezas` text NOT NULL,
  `codigointerno` varchar(30) NOT NULL,
  PRIMARY KEY (`idequipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'especificaciones' creation.
--

DROP TABLE IF EXISTS `especificaciones`;

CREATE TABLE IF NOT EXISTS `especificaciones` (
  `idespecificacion` int(11) NOT NULL AUTO_INCREMENT,
  `especificacion` varchar(150) NOT NULL,
  PRIMARY KEY (`idespecificacion`),
  UNIQUE KEY `especificacion` (`especificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'estado' creation.
--

DROP TABLE IF EXISTS `estado`;

CREATE TABLE IF NOT EXISTS `estado` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado`),
  UNIQUE KEY `estado` (`estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialaccion' creation.
--

DROP TABLE IF EXISTS `historialaccion`;

CREATE TABLE IF NOT EXISTS `historialaccion` (
  `idhistorialaccion` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombreaccion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`idhistorialaccion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialarea' creation.
--

DROP TABLE IF EXISTS `historialarea`;

CREATE TABLE IF NOT EXISTS `historialarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialequipo' creation.
--

DROP TABLE IF EXISTS `historialequipo`;

CREATE TABLE IF NOT EXISTS `historialequipo` (
  `idhistorial` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialequipo2' creation.
--

DROP TABLE IF EXISTS `historialequipo2`;

CREATE TABLE IF NOT EXISTS `historialequipo2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  `idEquipo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialespecificaciones' creation.
--

DROP TABLE IF EXISTS `historialespecificaciones`;

CREATE TABLE IF NOT EXISTS `historialespecificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialestado' creation.
--

DROP TABLE IF EXISTS `historialestado`;

CREATE TABLE IF NOT EXISTS `historialestado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialmarca' creation.
--

DROP TABLE IF EXISTS `historialmarca`;

CREATE TABLE IF NOT EXISTS `historialmarca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialmodelo' creation.
--

DROP TABLE IF EXISTS `historialmodelo`;

CREATE TABLE IF NOT EXISTS `historialmodelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialmotivo' creation.
--

DROP TABLE IF EXISTS `historialmotivo`;

CREATE TABLE IF NOT EXISTS `historialmotivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialpersonal' creation.
--

DROP TABLE IF EXISTS `historialpersonal`;

CREATE TABLE IF NOT EXISTS `historialpersonal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialproveedor' creation.
--

DROP TABLE IF EXISTS `historialproveedor`;

CREATE TABLE IF NOT EXISTS `historialproveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialtipo' creation.
--

DROP TABLE IF EXISTS `historialtipo`;

CREATE TABLE IF NOT EXISTS `historialtipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'historialusuario' creation.
--

DROP TABLE IF EXISTS `historialusuario`;

CREATE TABLE IF NOT EXISTS `historialusuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'marca' creation.
--

DROP TABLE IF EXISTS `marca`;

CREATE TABLE IF NOT EXISTS `marca` (
  `idmarca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  PRIMARY KEY (`idmarca`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'modelo' creation.
--

DROP TABLE IF EXISTS `modelo`;

CREATE TABLE IF NOT EXISTS `modelo` (
  `idmodelo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  PRIMARY KEY (`idmodelo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'motivo' creation.
--

DROP TABLE IF EXISTS `motivo`;

CREATE TABLE IF NOT EXISTS `motivo` (
  `idmotivo` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(50) NOT NULL,
  PRIMARY KEY (`idmotivo`),
  UNIQUE KEY `motivo` (`motivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'personal4' creation.
--

DROP TABLE IF EXISTS `personal4`;

CREATE TABLE IF NOT EXISTS `personal4` (
  `idpersonal` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `identificacion` varchar(50) NOT NULL,
  PRIMARY KEY (`idpersonal`),
  UNIQUE KEY `identificacion` (`identificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'pieza' creation.
--

DROP TABLE IF EXISTS `pieza`;

CREATE TABLE IF NOT EXISTS `pieza` (
  `idpieza` int(11) NOT NULL AUTO_INCREMENT,
  `codigointerno` varchar(30) NOT NULL,
  `parte` varchar(50) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idtipo` int(11) NOT NULL,
  `codigoexterno` varchar(50) NOT NULL,
  `idespecificacion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fechaasignacion` date NOT NULL,
  `fechacompra` date NOT NULL,
  `idarea` int(11) NOT NULL,
  `idasignado` int(11) NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idmodelo` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `idresponsable` int(11) NOT NULL,
  `numerofactura` varchar(50) NOT NULL,
  `disponibilidad` varchar(5) NOT NULL,
  `idaccion` int(11) NOT NULL,
  PRIMARY KEY (`idpieza`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'proveedor4' creation.
--

DROP TABLE IF EXISTS `proveedor4`;

CREATE TABLE IF NOT EXISTS `proveedor4` (
  `idproveedor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `rif` varchar(15) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `correo` varchar(75) NOT NULL,
  `direccion` text NOT NULL,
  PRIMARY KEY (`idproveedor`),
  UNIQUE KEY `rif` (`rif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'tipo' creation.
--

DROP TABLE IF EXISTS `tipo`;

CREATE TABLE IF NOT EXISTS `tipo` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





-- -----------------------------------------------------------------------------------------



--
-- Table 'usuario4' creation.
--

DROP TABLE IF EXISTS `usuario4`;

CREATE TABLE IF NOT EXISTS `usuario4` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `usuario` varchar(16) NOT NULL,
  `password` varchar(25) NOT NULL,
  `tipo` int(1) NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;


--
-- Dump data of `usuario4` 
--

INSERT INTO `usuario4` ( `idusuario`, `nombre`, `apellido`, `usuario`, `password`, `tipo` ) VALUES 
(1, 'Javierr', 'Delgado', 'admin', '19940338', 1),
(2, '', '', 'cheche', '19940338', 2),
(4, '', '', 'prueba', '19940338', 3),
(9, '', '', 'Mensito', '123456789', 1),
(10, '', '', 'armando', 'Maneiro1', 3),
(11, '', '', 'sahina', 'Ruizcampos', 2),
(13, '', '', 'manachito', 'manacho2', 2);



-- -------------------------------------------------------------------------------------------




SET FOREIGN_KEY_CHECKS=1;

