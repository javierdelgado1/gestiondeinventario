<?php
	session_start();
	if(!isset($_SESSION['usuario']))
		header('Location: index.html');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sistema de Gestion de Inventario</title>
	  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/formValidation.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="css/datePicker.css">
</head>
<body>

		<div class="container" style="width:900px;">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="#">Sistema de Inventario</a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
							<?php if($_SESSION['tipo']==1) { ?>

					        	<li class="active"><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home<span class="sr-only">(current)</span></a></li>
					       <!--  <li><a href="#">Link</a></li> -->
					        	<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu<span class="caret"></span></a>
					          <ul class="dropdown-menu" role="menu">
					            <li id="administraciondeusuarios"><a href="#"><span class="glyphicon glyphicon-user"></span>Administración de Usuarios</a></li>

					            <li id="administraciondeequiposypiezas" ><a href="#"><span class="glyphicon glyphicon-cog"></span>Administración de Equipos y Piezas</a></li>
					            <li id="reportes"><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes</a></li>

					            <li class="divider"></li>
					            <li id="configuraciondelsistema"><a href="#"><span class="glyphicon glyphicon-wrench"></span>Configuracion del Sistema</a></li>
					          
					            <li id="ayuda"><a href="#"><span class="glyphicon glyphicon-star"></span>Ayuda</a></li>

					            <?php }  if($_SESSION['tipo']==2) { ?>

					            	<li class="active"><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home<span class="sr-only">(current)</span></a></li>
					       <!--  <li><a href="#">Link</a></li> -->
					        	<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu<span class="caret"></span></a>
					          <ul class="dropdown-menu" role="menu">

					            <li id="administraciondeequiposypiezas" ><a href="#"><span class="glyphicon glyphicon-cog"></span>Administración de Equipos y Piezas</a></li>
					            <li id="reportes"><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes</a></li>

					            <li class="divider"></li>
					            <li id="configuraciondelsistema"><a href="#"><span class="glyphicon glyphicon-wrench"></span>Configuracion del Sistema</a></li>
					          
					            <li ><a href="#"><span class="glyphicon glyphicon-star"></span>Ayuda</a></li>

					            <?php }  if($_SESSION['tipo']==3) { ?>

					            	<li class="active"><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home<span class="sr-only">(current)</span></a></li>
					       <!--  <li><a href="#">Link</a></li> -->
					        	<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu<span class="caret"></span></a>
					          <ul class="dropdown-menu" role="menu">

					            <li id="administraciondeequiposypiezas" ><a href="#"><span class="glyphicon glyphicon-cog"></span>Administración de Equipos y Piezas</a></li>
					             <li id="reportes"><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes</a></li>

					            <li class="divider"></li>
					          
					            <li id="ayuda"><a href="#"><span class="glyphicon glyphicon-star"></span>Ayuda</a></li>

					            <?php } ?>
					          </ul>
					        </li>
					      </ul>

					      <ul class="nav navbar-nav navbar-right">
					        <li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-cog"></span>Configuracion<span class="caret"></span></a>
					          <ul class="dropdown-menu" role="menu">
					            <li data-toggle="modal" data-target="#modalcambiarpass"><a href="#"><span class="glyphicon glyphicon-wrench"></span>Cambiar Contraseña</a></li>
					            <li class="divider"></li>
					            <li id="cerrarsesion2"><a href="#"><span class="glyphicon glyphicon-off"></span>Cerrar Sesion</a></li>
					          </ul>
					        </li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
		</div>
		<div class="container" style="width:900px;">
			<div class="panel panel-default">
			  <div class="panel-heading">
						<b>Bienvenido</b> <?php 
							 						echo  $_SESSION['usuario'] ?>
							 				 <b>Tipo de usuario </b>: <?php   
							 				 			if($_SESSION['tipo']==1) 
							 				 				echo "administrador";
							 				 		    if($_SESSION['tipo']==2) 
							 				 		    	echo "Default";  
							 				 		    if($_SESSION['tipo']==3) 
							 				 		    	echo "Dpto Seguridad";
							 				 		?>		  		
					  	<div class="fecha navbar-right">
							  <b>Fecha </b><?php echo date('d-m-y');?>
							  <b>Reloj </b><div id="reloj" class="fecha"></div>
					  	
					  	</div>					
			  </div>
			  <div class="panel-body"  >
			  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
			  <section id="contenedor">
			  	
			  
			  	<?php 		if($_SESSION['tipo']==1)  { ?>
					  <div class="row">
					  <div class="col-md-4 col-xs-4 col-sm-4">
					  <a href="#administraciondeusuarios"  id="panelusuarios">
					  		<center><img src="img/user.png" alt="Administracion de Usuarios" class="img-rounded img-responsive" width="150" height="150">
					  		<b> Administración de Usuarios</b></center>
					  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
					  		  <a href="#administraciondeequiposypiezas"  id="panelequipos">
							  		<center><img src="img/piezasyequipos.png" alt="Administracion de Equipos y Piezas" class="img-rounded" width="230" height="150"><br>
							  		<b>Administración de Equipos y Piezas</b></center>
							  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
					  		 <a href="#reportes"  id="panelreportes">
							  		<center><img src="img/reporte2.png" alt="Reportes" class="img-rounded img-responsive" width="150" height="150">
							  		<b>Reportes</b></center>
							  </a>
					  </div>			  
					</div>
					<br><br>
					<hr>
					<div class="row">
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
					  	 	<a href="#Configuraciondelsistema"  id="panelconfiguraciondelsistema">
							  		<center><img src="img/configuracion.png" alt="Configuración del Sistema" class="img-rounded img-responsive" width="150" height="150">
							  		<b>Configuracion del Sistema</b></center>
							  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
								<a href="#ayuda"  id="panelayuda">
							  		<center><img src="img/ayuda.png" alt="ayuda" class="img-rounded img-responsive" width="150" height="150">
							  		<b>Ayuda</b></center>
							  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
								<a href="#salir"  id="panelsalir">
							  		<center><img src="img/cerrar.png" alt="Salir" class="img-rounded img-responsive" width="120" height="120">
							  		<br><b>Salir </b></center>
							  </a>
					  </div>			  
					</div>
					<?php } 	if($_SESSION['tipo']==2) {  ?>
									<div class="row">
									 
									  <div class="col-md-4 col-xs-4 col-sm-4">					  
									  		  <a href="#administraciondeequiposypiezas"  id="panelequipos">
											  		<center><img src="img/piezasyequipos.png" alt="Administracion de Equipos y Piezas" class="img-rounded" width="230" height="150"><br>
											  		<b>Administración de Equipos y Piezas</b></center>
											  </a>
									  </div>
									  <div class="col-md-4 col-xs-4 col-sm-4">					  
									  		 <a href="#reportes"  id="panelreportes">
											  		<center><img src="img/reporte2.png" alt="Reportes" class="img-rounded img-responsive" width="150" height="150">
											  		<b>Reportes</b></center>
											  </a>
									  </div>			  
									  <div class="col-md-4 col-xs-4 col-sm-4">					  
									  	 	<a href="#Configuraciondelsistema"  id="panelconfiguraciondelsistema">
											  		<center><img src="img/configuracion.png" alt="Configuración del Sistema" class="img-rounded img-responsive" width="150" height="150">
											  		<b>Configuracion del Sistema</b></center>
											  </a>
									  </div>
									</div>
									<br><br>
									<hr>
									<div class="row">
									  <!-- <div class="col-md-4 col-xs-4 col-sm-4">					  
									  	 	<a href="#Configuraciondelsistema"  id="panelconfiguraciondelsistema">
											  		<center><img src="img/configuracion.png" alt="Configuración del Sistema" class="img-rounded img-responsive" width="150" height="150">
											  		<b>Configuracion del Sistema</b></center>
											  </a>
									  </div> -->
									  <div class="col-md-4 col-xs-4 col-sm-4">					  
												<a href="#ayuda"  id="panelayuda">
											  		<center><img src="img/ayuda.png" alt="ayuda" class="img-rounded img-responsive" width="150" height="150">
											  		<b>Ayuda</b></center>
											  </a>
									  </div>
									  <div class="col-md-4 col-xs-4 col-sm-4">					  
												<a href="#salir"  id="panelsalir">
											  		<center><img src="img/cerrar.png" alt="Salir" class="img-rounded img-responsive" width="120" height="120">
											  		<br><b>Salir </b></center>
											  </a>
									  </div>			  
									</div>




					<?php } 	if($_SESSION['tipo']==3)  {  ?>

					<div class="row">
					 
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
					  		  <a href="#administraciondeequiposypiezas"  id="panelequipos">
							  		<center><img src="img/piezasyequipos.png" alt="Administracion de Equipos y Piezas" class="img-rounded" width="230" height="150"><br>
							  		<b>Administración de Equipos y Piezas</b></center>
							  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
					  		 <a href="#reportes"  id="panelreportes">
							  		<center><img src="img/reporte2.png" alt="Reportes" class="img-rounded img-responsive" width="150" height="150">
							  		<b>Reportes</b></center>
							  </a>
					  </div>
					  <div class="col-md-4 col-xs-4 col-sm-4">					  
								<a href="#salir"  id="panelsalir">
							  		<center><img src="img/cerrar.png" alt="Salir" class="img-rounded img-responsive" width="120" height="120">
							  		<br><b>Salir </b></center>
							  </a>
					  </div>			  
					</div>
					<div class="row">
						<div class="col-md-4 col-xs-4 col-sm-4">					  
									<a href="#ayuda"  id="panelayuda2">
								  		<center><img src="img/ayuda.png" alt="ayuda" class="img-rounded img-responsive" width="150" height="150">
								  		<b>Ayuda</b></center>
								  </a>
						  </div>
					  </div>



					<?php } ?>
					</section>
			  </div>
			</div>
			
		</div>

<div class="modal fade" id="modalcambiarpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cambiar Contraseña</h4>
      </div>
      <div class="modal-body">
        <form id="formcambiarpassword" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 
        					<div class="form-group">
					          <label class="col-sm-4 control-label">idUsario:</label>
					          <div class="col-sm-6">
					                <input type="text" class="form-control input-sm" value=<?php  echo $_SESSION['idusuario'];?>  disabled>
					          </div>
					      	</div> 
					      	<div class="form-group">
					          <label class="col-sm-4 control-label">Nombre de usuario:</label>
					          <div class="col-sm-6">
					                <input id="tc" type="text" class="form-control input-sm"  name="ci" value=<?php  echo $_SESSION['usuario'];  ?> disabled >
					          </div>
					      	</div> 
        					<div class="form-group">
					          <label class="col-sm-4 control-label">Contraseña:</label>
					          <div class="col-sm-6">
					                <input id="pass1" type="password" class="form-control input-sm"  name="password" >
					          </div>
					      	</div> 
					      	<div class="form-group">
					          <label class="col-sm-4 control-label">Confirmar:</label>
					          <div class="col-sm-6">
					                <input id="pass2" type="password" class="form-control input-sm"  name="confirmPassword">
					          </div>
					      	</div> 
		      	 <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary" id="cambiarContrasena">Aceptar</button>
		      	</div>
	         </form>       
      </div>
     
    </div>
  </div>
</div>

	 <script src="js/jquery-2.1.1.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

    <script src="js/eventos2.js"></script>
	<script type="text/javascript"> eventos(); reloj(1);</script> 

	<script type="text/javascript" src="js/bootstrapValidator.js"></script>
	<script src="js/bootstrapValidator.min.js"></script>
    <script src="js/date.js"></script>	
    <script src="js/jquery.datePicker.js"></script>


	<script type="text/javascript">
	$(document).ready(function() {
		    $('#formcambiarpassword').bootstrapValidator({
			        framework: 'bootstrap',
			        icon: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			        	password:{
			        		validators: {
			                	notEmpty: {
			                            message: 'Se requiere este campo'
			                     },
			                     regexp: {
				                        regexp: /^[a-zA-Z0-9]*$/,
				                        message: 'Solo se permiten contraseñas con letras y numeros'
				                    }
			        	}
			        },
			            confirmPassword: {
			                validators: {
			                	notEmpty: {
			                            message: 'Se requiere este campo'
			                     },
			                    identical: {
			                        field: 'password',
			                        message: 'La Contraseña tienen que ser iguales'
			                    }
			                }
			            }
			        }
			    }).on('err.field.fv', function(e, data) {
		            // $(e.target)  --> The field element
		            // data.fv      --> The FormValidation instance
		            // data.field   --> The field name
		            // data.element --> The field element

		           data.fv.disableSubmitButtons(false);
		          console.log("deshabilito boton");
		        })
		        .on('success.field.fv', function(e, data) {
		            // e, data parameters are the same as in err.field.fv event handler
		            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
		            // - The submit button is clicked
		            // - The form is invalid
		           data.fv.disableSubmitButtons(false);
		          console.log("habilito boton");

		        });

    $('#cambiarContrasena').on('click',  function(){
		$.ajax
			({
			type: "POST",
		   	url: "modelo/consultarusuario.php",
		   	data: {id:8, pass:pass2.value},
			async: false,	
			dataType: "json",	
			success:
		    function (msg) 
			{	

				$('#modalcambiarpass').modal('hide');
				
		     },
		        error:
		        function (msg) {console.log( msg +"  No se pudo realizar la conexion");}
			});
	});
		    });
		menuBarra();
function menuBarra(){
		$('#administraciondeusuarios').on('click',  function(){
		console.log("click en panelusuarios");
		$("#contenedor").hide().load('partials/administrador/gestiondeusuarios.html', function(){			
			
		}).fadeIn(1500);
	});
	$('#configuraciondelsistema').on('click',  function(){
		console.log("click en panelconfiguraciondelsistema");
		$("#contenedor").hide().load('partials/administrador/configuraciondelsistema.php', function(){			
			
		}).fadeIn(1500);
	});
	$('#administraciondeequiposypiezas').on('click',  function(){
		console.log("click en panelequipos");
		$("#contenedor").hide().load('partials/administrador/administraciondeequiposypiezas.php', function(){			
			
		}).fadeIn(1500);
	});
	$('#reportes').on('click',  function(){
		console.log("click en panelreportes");
		$("#contenedor").hide().load('partials/administrador/reportes.html', function(){			
			
		}).fadeIn(1500);
	});
	$('#ayuda').on('click',  function(){
		console.log("click en panelayuda");
		$("#contenedor").hide().load('partials/administrador/ayuda.html', function(){			
			
		}).fadeIn(1500);
	});
  $('#cerrarsesion2').on('click',  function(){

	  	console.log("click en cerrar sesion");
	  	$.ajax
			({
			type: "POST",
		   	url: "modelo/consultarusuario.php",
		   	data: {id:3},
			async: false,	
				
			success:
		    function () 
			{	
				
				window.open('index.html' , '_self');
		     },
		        error:
		        function () {console.log( msg +"  No se pudo realizar la conexion");}
			});
    });
}
	</script> 
</body>
</html>