-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2015 a las 01:05:14
-- Versión del servidor: 5.6.15-log
-- Versión de PHP: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gestiondeinventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accion`
--

CREATE TABLE IF NOT EXISTS `accion` (
  `idaccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idaccion`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `accion`
--

INSERT INTO `accion` (`idaccion`, `nombre`) VALUES
(5, 'Almacenamiento'),
(3, 'Asignacion'),
(6, 'Crear'),
(4, 'Desincorporar'),
(1, 'Mantenimiento Preventivo'),
(2, 'Reconfiguracion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `idareas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`idareas`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`idareas`, `nombre`, `descripcion`, `codigo`) VALUES
(1, 'Almacen', 'Almacen de Tecnologia', 'A-2-7'),
(2, 'Taller', 'Taller de Tecnologia', 'A-2-5'),
(3, 'Deposito', 'Depositto General', 'A-0-9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cargo` (`cargo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `cargo`) VALUES
(3, 'Administrador de Sistemas'),
(1, 'Jefe de Seguridad'),
(2, 'Tecnico de Sistemas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE IF NOT EXISTS `equipo` (
  `idtipo` varchar(50) NOT NULL,
  `idequipo` int(11) NOT NULL AUTO_INCREMENT,
  `idaccion` int(11) NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idmodelo` int(11) NOT NULL,
  `idespecificacion` int(11) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idmotivo` int(11) NOT NULL,
  `idasignadopor` int(11) NOT NULL,
  `idresponsablenuevo` int(11) NOT NULL,
  `personal` int(11) NOT NULL,
  `idarea` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fechacompra` date NOT NULL,
  `fechaasignacion` date NOT NULL,
  `fecharealizado` date NOT NULL,
  `codigoexterno` varchar(50) NOT NULL,
  `numerofactura` varchar(3) NOT NULL,
  `piezas` text NOT NULL,
  `codigointerno` varchar(30) NOT NULL,
  PRIMARY KEY (`idequipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`idtipo`, `idequipo`, `idaccion`, `idmarca`, `idmodelo`, `idespecificacion`, `idestado`, `idmotivo`, `idasignadopor`, `idresponsablenuevo`, `personal`, `idarea`, `idproveedor`, `fechacompra`, `fechaasignacion`, `fecharealizado`, `codigoexterno`, `numerofactura`, `piezas`, `codigointerno`) VALUES
('8', 1, 0, 1, 2, 24, 1, 0, 0, 0, 0, 1, 1, '2015-03-31', '0000-00-00', '0000-00-00', 'DOP32343', '1', '-1', 'DeCoCo1'),
('27', 2, 0, 1, 3, 13, 1, 0, 0, 0, 0, 1, 1, '2015-02-04', '0000-00-00', '0000-00-00', 'DINS2343', '1', '-1', 'DeLa172'),
('25', 3, 0, 1, 1, 25, 1, 0, 0, 0, 0, 1, 1, '2015-02-04', '0000-00-00', '0000-00-00', '8812837', '4', 'a:1:{i:0;s:1:"4";}', 'DeSeT33'),
('28', 4, 0, 8, 1, 27, 1, 0, 0, 0, 0, 1, 1, '2015-02-04', '0000-00-00', '0000-00-00', '5556526', '5', 'a:1:{i:0;s:1:"5";}', 'HPSe344'),
('8', 5, 0, 1, 2, 24, 1, 0, 0, 0, 0, 1, 1, '2015-03-31', '0000-00-00', '0000-00-00', 'KJHKU8788778', '5', '-1', 'DeCoCo5'),
('9', 6, 0, 1, 2, 23, 1, 0, 0, 0, 0, 1, 1, '2015-02-04', '0000-00-00', '0000-00-00', 'HJJ66533', '6', '-1', 'DeCoP46'),
('8', 7, 0, 1, 2, 24, 1, 0, 0, 0, 0, 1, 1, '2015-04-01', '0000-00-00', '0000-00-00', '98874JHHJG', '6', '-1', 'DeCoCo7'),
('27', 8, 0, 1, 3, 27, 1, 0, 0, 0, 0, 1, 1, '2015-04-08', '0000-00-00', '0000-00-00', '89798J', '7', '-1', 'DeLa348'),
('11', 9, 0, 5, 2, 24, 1, 0, 0, 0, 0, 1, 1, '2015-03-31', '0000-00-00', '0000-00-00', 'MNBMN888', '8', '-1', 'ASCoCo9'),
('8', 10, 0, 5, 2, 24, 1, 0, 0, 0, 0, 1, 1, '2015-03-30', '0000-00-00', '0000-00-00', 'MNNM6555', '9', '-1', 'ASCoCo10'),
('9', 11, 0, 1, 2, 23, 1, 0, 0, 0, 0, 1, 1, '2015-04-06', '0000-00-00', '0000-00-00', 'KJG66666', '11', 'a:1:{i:0;s:1:"6";}', 'DeCoP411'),
('25', 12, 1, 3, 2, 24, 1, 0, 0, 0, 2, 1, 1, '2015-03-05', '0000-00-00', '2015-04-01', '5564JH', '12', 'a:1:{i:0;s:1:"2";}', 'ATCoCo12'),
('25', 13, 0, 1, 1, 25, 1, 0, 0, 0, 0, 1, 1, '2015-04-02', '0000-00-00', '0000-00-00', '654654', '60', 'a:1:{i:0;s:1:"3";}', 'DeSeT313');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especificaciones`
--

CREATE TABLE IF NOT EXISTS `especificaciones` (
  `idespecificacion` int(11) NOT NULL AUTO_INCREMENT,
  `especificacion` varchar(150) NOT NULL,
  PRIMARY KEY (`idespecificacion`),
  UNIQUE KEY `especificacion` (`especificacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `especificaciones`
--

INSERT INTO `especificaciones` (`idespecificacion`, `especificacion`) VALUES
(26, ''),
(12, '15"'),
(18, '160Gb'),
(13, '17"'),
(14, '19"'),
(1, '1gb'),
(6, '1Gb 800Mhz'),
(20, '1Tb'),
(15, '21"'),
(2, '2Gb'),
(9, '2Gb 1666Mhz'),
(5, '2Gb 800Mhz'),
(19, '320Gb'),
(27, '3400S'),
(16, '40Gb'),
(4, '4Gb'),
(8, '4gb 1666Mhz'),
(7, '4Gb 800Mhz'),
(11, '500va 2 salidas reguladas'),
(3, '512Mb'),
(10, '750w 4 salidas reguladas'),
(17, '80Gb'),
(24, 'Core2Duo 3.2Ghz 4Gb/DVD-ROM'),
(21, 'Espanol'),
(22, 'Ingles'),
(28, 'matriz de puntos'),
(23, 'P4 3.0 2Gb Ram/CD-ROM'),
(25, 'T320');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado`),
  UNIQUE KEY `estado` (`estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idestado`, `estado`) VALUES
(8, 'Da?ado'),
(5, 'Desincorporado'),
(3, 'En reserva'),
(7, 'En taller'),
(1, 'Nuevo sin uso'),
(2, 'Operativo'),
(4, 'Por reparar'),
(6, 'Por revisar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialaccion`
--

CREATE TABLE IF NOT EXISTS `historialaccion` (
  `idhistorialaccion` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombreaccion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`idhistorialaccion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialarea`
--

CREATE TABLE IF NOT EXISTS `historialarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `historialarea`
--

INSERT INTO `historialarea` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Taller', '2015-04-09', '10:59:11'),
(2, 'crea', 'mlarosa', 'Almacen', '2015-04-09', '10:59:45'),
(3, 'crea', 'mlarosa', 'Deposito', '2015-04-09', '11:00:11'),
(4, 'crea', 'mlarosa', 'Almacen ', '2015-04-09', '11:47:06'),
(5, 'crea', 'mlarosa', 'Taller', '2015-04-09', '11:47:23'),
(6, 'crea', 'mlarosa', 'Deposito', '2015-04-09', '11:47:42'),
(7, 'crea', 'mlarosa', 'Almacen', '2015-04-09', '12:02:00'),
(8, 'crea', 'mlarosa', 'Taller', '2015-04-09', '12:02:37'),
(9, 'crea', 'mlarosa', 'Deposito', '2015-04-09', '12:03:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialequipo`
--

CREATE TABLE IF NOT EXISTS `historialequipo` (
  `idhistorial` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `historialequipo`
--

INSERT INTO `historialequipo` (`idhistorial`, `accion`, `idusuario`, `idequipo`, `Fecha`, `hora`) VALUES
(2, '6', 14, 2, '2015-04-09', '11:54:48'),
(3, '6', 14, 1, '2015-04-09', '12:04:36'),
(4, '6', 14, 2, '2015-04-09', '12:24:25'),
(5, '6', 14, 3, '2015-04-09', '12:25:46'),
(6, '6', 14, 4, '2015-04-09', '12:27:21'),
(7, '6', 14, 5, '2015-04-09', '12:30:58'),
(8, '6', 14, 6, '2015-04-09', '12:31:48'),
(9, '6', 14, 7, '2015-04-09', '12:32:21'),
(10, '6', 14, 8, '2015-04-09', '12:33:15'),
(11, '6', 14, 9, '2015-04-09', '12:34:06'),
(12, '6', 14, 10, '2015-04-09', '12:34:53'),
(13, '6', 14, 11, '2015-04-09', '12:35:50'),
(14, '6', 14, 12, '2015-04-09', '12:36:26'),
(15, '2', 14, 12, '2015-04-09', '13:25:49'),
(16, '1', 14, 12, '2015-04-09', '13:26:49'),
(17, '6', 14, 13, '2015-04-09', '17:21:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialequipo2`
--

CREATE TABLE IF NOT EXISTS `historialequipo2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  `idEquipo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `historialequipo2`
--

INSERT INTO `historialequipo2` (`id`, `accion`, `ejecuta`, `fecha`, `hora`, `idEquipo`) VALUES
(2, 'crea', 'mlarosa', '2015-04-09', '11:54:48', 'DeCoP42'),
(3, 'crea', 'mlarosa', '2015-04-09', '12:04:36', 'DeCoCo1'),
(4, 'crea', 'mlarosa', '2015-04-09', '12:24:25', 'DeLa172'),
(5, 'crea', 'mlarosa', '2015-04-09', '12:25:46', 'DeSeT33'),
(6, 'crea', 'mlarosa', '2015-04-09', '12:27:21', 'HPSe344'),
(7, 'crea', 'mlarosa', '2015-04-09', '12:30:58', 'DeCoCo5'),
(8, 'crea', 'mlarosa', '2015-04-09', '12:31:48', 'DeCoP46'),
(9, 'crea', 'mlarosa', '2015-04-09', '12:32:21', 'DeCoCo7'),
(10, 'crea', 'mlarosa', '2015-04-09', '12:33:15', 'DeLa348'),
(11, 'crea', 'mlarosa', '2015-04-09', '12:34:06', 'ASCoCo9'),
(12, 'crea', 'mlarosa', '2015-04-09', '12:34:53', 'ASCoCo10'),
(13, 'crea', 'mlarosa', '2015-04-09', '12:35:50', 'DeCoP411'),
(14, 'crea', 'mlarosa', '2015-04-09', '12:36:26', 'ATCoCo12'),
(15, 'actualiza', 'mlarosa', '2015-04-09', '13:25:49', 'ATCoCo12'),
(16, 'actualiza', 'mlarosa', '2015-04-09', '13:26:49', 'ATCoCo12'),
(17, 'crea', 'mlarosa', '2015-04-09', '17:21:16', 'DeSeT313');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialespecificaciones`
--

CREATE TABLE IF NOT EXISTS `historialespecificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `historialespecificaciones`
--

INSERT INTO `historialespecificaciones` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', '1gb', '2015-04-09', '11:11:57'),
(2, 'crea', 'mlarosa', '2Gb', '2015-04-09', '11:12:01'),
(3, 'crea', 'mlarosa', '512Mb', '2015-04-09', '11:12:14'),
(4, 'crea', 'mlarosa', '4Gb', '2015-04-09', '11:12:22'),
(5, 'crea', 'mlarosa', '2Gb 800Mhz', '2015-04-09', '11:12:32'),
(6, 'crea', 'mlarosa', '1Gb 800Mhz', '2015-04-09', '11:12:41'),
(7, 'crea', 'mlarosa', '4Gb 800Mhz', '2015-04-09', '11:13:00'),
(8, 'crea', 'mlarosa', '4gb 1666Mhz', '2015-04-09', '11:13:12'),
(9, 'crea', 'mlarosa', '2Gb 1666Mhz', '2015-04-09', '11:13:22'),
(10, 'crea', 'mlarosa', '750w 4 salidas reguladas', '2015-04-09', '11:13:39'),
(11, 'crea', 'mlarosa', '500va 2 salidas reguladas', '2015-04-09', '11:13:50'),
(12, 'crea', 'mlarosa', '15"', '2015-04-09', '11:13:56'),
(13, 'crea', 'mlarosa', '17"', '2015-04-09', '11:14:02'),
(14, 'crea', 'mlarosa', '19"', '2015-04-09', '11:14:07'),
(15, 'crea', 'mlarosa', '21"', '2015-04-09', '11:14:11'),
(16, 'crea', 'mlarosa', '40Gb', '2015-04-09', '11:14:20'),
(17, 'crea', 'mlarosa', '80Gb', '2015-04-09', '11:14:25'),
(18, 'crea', 'mlarosa', '160Gb', '2015-04-09', '11:14:39'),
(19, 'crea', 'mlarosa', '320Gb', '2015-04-09', '11:14:45'),
(20, 'crea', 'mlarosa', '1Tb', '2015-04-09', '11:14:51'),
(21, 'crea', 'mlarosa', 'Espanol', '2015-04-09', '11:20:39'),
(22, 'crea', 'mlarosa', 'Ingles', '2015-04-09', '11:20:44'),
(23, 'crea', 'mlarosa', 'P4 3.0 2Gb Ram/CD-ROM', '2015-04-09', '11:23:38'),
(24, 'crea', 'mlarosa', 'Core2Duo 3.2Ghz 4Gb/DVD-ROM', '2015-04-09', '11:24:06'),
(25, 'crea', 'mlarosa', 'T320', '2015-04-09', '11:26:33'),
(26, 'crea', 'mlarosa', '', '2015-04-09', '12:23:27'),
(27, 'crea', 'mlarosa', '3400S', '2015-04-09', '12:26:35'),
(28, 'crea', 'mlarosa', 'matriz de puntos', '2015-04-09', '17:18:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialestado`
--

CREATE TABLE IF NOT EXISTS `historialestado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `historialestado`
--

INSERT INTO `historialestado` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Nuevo sin uso', '2015-04-09', '11:01:09'),
(2, 'crea', 'mlarosa', 'Operativo', '2015-04-09', '11:01:18'),
(3, 'crea', 'mlarosa', 'DaÃ±ado', '2015-04-09', '11:01:25'),
(4, 'crea', 'mlarosa', 'Desincorporado', '2015-04-09', '11:01:58'),
(5, 'crea', 'mlarosa', 'En Reparacion', '2015-04-09', '11:02:10'),
(6, 'crea', 'mlarosa', 'Operando', '2015-04-09', '11:02:42'),
(7, 'crea', 'mlarosa', 'Danado', '2015-04-09', '11:03:16'),
(8, 'crea', 'mlarosa', 'Almacenado', '2015-04-09', '11:03:26'),
(9, 'crea', 'mlarosa', 'Nuevo sin uso', '2015-04-09', '11:50:04'),
(10, 'crea', 'mlarosa', 'Operativo', '2015-04-09', '11:50:10'),
(11, 'crea', 'mlarosa', 'En reserva', '2015-04-09', '11:50:18'),
(12, 'crea', 'mlarosa', 'Por reparar', '2015-04-09', '11:50:27'),
(13, 'crea', 'mlarosa', 'Desincorporado', '2015-04-09', '11:50:37'),
(14, 'crea', 'mlarosa', 'Por revisar', '2015-04-09', '11:50:45'),
(15, 'crea', 'mlarosa', 'En taller', '2015-04-09', '11:50:51'),
(16, 'crea', 'mlarosa', 'Da?ado', '2015-04-09', '11:51:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialmarca`
--

CREATE TABLE IF NOT EXISTS `historialmarca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `historialmarca`
--

INSERT INTO `historialmarca` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Dell', '2015-04-09', '11:06:00'),
(2, 'crea', 'mlarosa', 'Corsair', '2015-04-09', '11:06:11'),
(3, 'crea', 'mlarosa', 'ATI', '2015-04-09', '11:06:25'),
(4, 'crea', 'mlarosa', 'APC', '2015-04-09', '11:06:35'),
(5, 'crea', 'mlarosa', 'ASUS', '2015-04-09', '11:06:50'),
(6, 'crea', 'mlarosa', 'HITACHI', '2015-04-09', '11:07:37'),
(7, 'crea', 'mlarosa', 'EPSON', '2015-04-09', '12:20:38'),
(8, 'crea', 'mlarosa', 'HP', '2015-04-09', '12:26:10'),
(9, 'crea', 'mlarosa', 'CISCO', '2015-04-09', '17:16:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialmodelo`
--

CREATE TABLE IF NOT EXISTS `historialmodelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `historialmodelo`
--

INSERT INTO `historialmodelo` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Servidor', '2015-04-09', '10:55:52'),
(2, 'crea', 'mlarosa', 'Computador', '2015-04-09', '10:56:14'),
(3, 'crea', 'mlarosa', 'Laptop', '2015-04-09', '10:56:32'),
(4, 'crea', 'mlarosa', 'Tarjeta de Video', '2015-04-09', '10:56:57'),
(5, 'crea', 'mlarosa', 'Tarjeta Madre', '2015-04-09', '10:57:12'),
(6, 'crea', 'mlarosa', 'Ups', '2015-04-09', '10:57:40'),
(7, 'crea', 'mlarosa', 'Monitor', '2015-04-09', '10:57:54'),
(8, 'crea', 'mlarosa', 'Teclado', '2015-04-09', '10:58:07'),
(9, 'crea', 'mlarosa', 'Mouse', '2015-04-09', '10:58:28'),
(10, 'crea', 'mlarosa', 'Impresora', '2015-04-09', '12:13:22'),
(11, 'crea', 'mlarosa', 'HDD', '2015-04-09', '12:18:49'),
(12, 'crea', 'mlarosa', 'Procesador', '2015-04-09', '12:19:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialmotivo`
--

CREATE TABLE IF NOT EXISTS `historialmotivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `historialmotivo`
--

INSERT INTO `historialmotivo` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Fin de vida util', '2015-04-09', '11:04:24'),
(2, 'crea', 'mlarosa', 'Falla sin solucionar', '2015-04-09', '11:04:37'),
(3, 'crea', 'mlarosa', 'Funcionamiento Irregular', '2015-04-09', '11:04:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialpersonal`
--

CREATE TABLE IF NOT EXISTS `historialpersonal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `historialpersonal`
--

INSERT INTO `historialpersonal` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'Manuel', '2015-04-09', '11:03:50'),
(2, 'crea', 'mlarosa', 'Marcelo ', '2015-04-09', '11:04:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialproveedor`
--

CREATE TABLE IF NOT EXISTS `historialproveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `historialproveedor`
--

INSERT INTO `historialproveedor` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'newegg', '2015-04-09', '11:05:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialtipo`
--

CREATE TABLE IF NOT EXISTS `historialtipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `historialtipo`
--

INSERT INTO `historialtipo` (`id`, `accion`, `ejecuta`, `nombre`, `fecha`, `hora`) VALUES
(1, 'crea', 'mlarosa', 'SSD', '2015-04-09', '11:07:54'),
(2, 'crea', 'mlarosa', 'SATA', '2015-04-09', '11:07:58'),
(3, 'crea', 'mlarosa', 'IDE', '2015-04-09', '11:08:02'),
(4, 'crea', 'mlarosa', 'LCD', '2015-04-09', '11:08:07'),
(5, 'crea', 'mlarosa', 'CRT', '2015-04-09', '11:08:11'),
(6, 'crea', 'mlarosa', 'OPTIPLEX', '2015-04-09', '11:08:18'),
(7, 'crea', 'mlarosa', 'INSPIRON', '2015-04-09', '11:08:23'),
(8, 'crea', 'mlarosa', 'OPTIPLEX 755', '2015-04-09', '11:09:14'),
(9, 'crea', 'mlarosa', 'OPTIPLEX 740', '2015-04-09', '11:09:26'),
(10, 'crea', 'mlarosa', 'VOSTR 230', '2015-04-09', '11:09:34'),
(11, 'crea', 'mlarosa', 'CLON LAB', '2015-04-09', '11:10:16'),
(12, 'crea', 'mlarosa', 'ALL IN ONE', '2015-04-09', '11:10:30'),
(13, 'crea', 'mlarosa', '500va', '2015-04-09', '11:10:55'),
(14, 'crea', 'mlarosa', '750va', '2015-04-09', '11:11:00'),
(15, 'crea', 'mlarosa', 'DDR2', '2015-04-09', '11:11:19'),
(16, 'crea', 'mlarosa', 'DDR', '2015-04-09', '11:11:24'),
(17, 'crea', 'mlarosa', 'DDR3', '2015-04-09', '11:11:29'),
(18, 'crea', 'mlarosa', 'HD 4500', '2015-04-09', '11:11:37'),
(19, 'crea', 'mlarosa', 'HD 8750', '2015-04-09', '11:11:47'),
(20, 'crea', 'mlarosa', 'HDD', '2015-04-09', '11:15:17'),
(21, 'crea', 'mlarosa', 'USB HDD', '2015-04-09', '11:15:33'),
(22, 'crea', 'mlarosa', 'AGP', '2015-04-09', '11:16:48'),
(23, 'crea', 'mlarosa', 'Socket 1055 a/v/r', '2015-04-09', '11:17:57'),
(24, 'crea', 'mlarosa', 'USB', '2015-04-09', '11:20:17'),
(25, 'crea', 'mlarosa', 'POWEREDGE', '2015-04-09', '11:26:09'),
(26, 'crea', 'mlarosa', 'LX3100', '2015-04-09', '12:21:07'),
(27, 'crea', 'mlarosa', 'INSPIRON', '2015-04-09', '12:23:10'),
(28, 'crea', 'mlarosa', 'PROLIANT', '2015-04-09', '12:26:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialusuario`
--

CREATE TABLE IF NOT EXISTS `historialusuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(50) NOT NULL,
  `ejecuta` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `historialusuario`
--

INSERT INTO `historialusuario` (`id`, `accion`, `ejecuta`, `usuario`, `fecha`, `hora`) VALUES
(1, 'creado', 'admin', 'mlarosa', '2015-04-09', '10:54:25'),
(2, 'creado', 'mlarosa', 'aquiles', '2015-04-09', '17:26:37'),
(3, 'creado', 'mlarosa', 'laurab', '2015-04-09', '17:27:22'),
(4, 'modificado', 'mlarosa', 'laurab', '2015-04-09', '17:28:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE IF NOT EXISTS `marca` (
  `idmarca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  PRIMARY KEY (`idmarca`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`, `imagen`) VALUES
(1, 'Dell', 'Coverc4ca4238a0b923820dcc509a6f75849b.jpg'),
(2, 'Corsair', 'Coverc81e728d9d4c2f636f067f89cc14862c.jpg'),
(3, 'ATI', 'Covereccbc87e4b5ce2fe28308fd9f2a7baf3.jpg'),
(4, 'APC', 'Covera87ff679a2f3e71d9181a67b7542122c.jpg'),
(5, 'ASUS', 'Covere4da3b7fbbce2345d7772b0674a318d5.jpg'),
(6, 'HITACHI', 'Cover1679091c5a880faf6fb5e6087eb1b2dc.jpg'),
(7, 'EPSON', 'Cover8f14e45fceea167a5a36dedd4bea2543.jpg'),
(8, 'HP', 'Coverc9f0f895fb98ab9159f51fd0297e236d.jpg'),
(9, 'CISCO', 'Cover45c48cce2e2d7fbdea1afc51c7c6ad26.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo`
--

CREATE TABLE IF NOT EXISTS `modelo` (
  `idmodelo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  PRIMARY KEY (`idmodelo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `modelo`
--

INSERT INTO `modelo` (`idmodelo`, `nombre`, `imagen`) VALUES
(1, 'Servidor', 'Coverc4ca4238a0b923820dcc509a6f75849b.jpg'),
(2, 'Computador', 'Coverc81e728d9d4c2f636f067f89cc14862c.jpg'),
(3, 'Laptop', 'Covereccbc87e4b5ce2fe28308fd9f2a7baf3.jpg'),
(4, 'Tarjeta de Video', 'Covera87ff679a2f3e71d9181a67b7542122c.jpg'),
(5, 'Tarjeta Madre', 'Covere4da3b7fbbce2345d7772b0674a318d5.jpg'),
(6, 'Ups', 'Cover1679091c5a880faf6fb5e6087eb1b2dc.jpg'),
(7, 'Monitor', 'Cover8f14e45fceea167a5a36dedd4bea2543.jpg'),
(8, 'Teclado', 'Coverc9f0f895fb98ab9159f51fd0297e236d.jpg'),
(9, 'Mouse', 'Cover45c48cce2e2d7fbdea1afc51c7c6ad26.jpg'),
(10, 'Impresora', 'Coverd3d9446802a44259755d38e6d163e820.jpg'),
(11, 'HDD', 'Cover6512bd43d9caa6e02c990b0a82652dca.jpg'),
(12, 'Procesador', 'Coverc20ad4d76fe97759aa27a0c99bff6710.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo`
--

CREATE TABLE IF NOT EXISTS `motivo` (
  `idmotivo` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(50) NOT NULL,
  PRIMARY KEY (`idmotivo`),
  UNIQUE KEY `motivo` (`motivo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `motivo`
--

INSERT INTO `motivo` (`idmotivo`, `motivo`) VALUES
(2, 'Falla sin solucionar'),
(1, 'Fin de vida util'),
(3, 'Funcionamiento Irregular');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal4`
--

CREATE TABLE IF NOT EXISTS `personal4` (
  `idpersonal` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `identificacion` varchar(50) NOT NULL,
  PRIMARY KEY (`idpersonal`),
  UNIQUE KEY `identificacion` (`identificacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `personal4`
--

INSERT INTO `personal4` (`idpersonal`, `nombre`, `apellido`, `cargo`, `identificacion`) VALUES
(1, 'Manuel', 'La Rosa', '2', '14432073'),
(2, 'Marcelo ', 'Pacheco', '3', '7333222');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pieza`
--

CREATE TABLE IF NOT EXISTS `pieza` (
  `idpieza` int(11) NOT NULL AUTO_INCREMENT,
  `codigointerno` varchar(30) NOT NULL,
  `parte` varchar(50) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idtipo` int(11) NOT NULL,
  `codigoexterno` varchar(50) NOT NULL,
  `idespecificacion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fechaasignacion` date NOT NULL,
  `fechacompra` date NOT NULL,
  `idarea` int(11) NOT NULL,
  `idasignado` int(11) NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idmodelo` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `idresponsable` int(11) NOT NULL,
  `numerofactura` varchar(50) NOT NULL,
  `disponibilidad` varchar(5) NOT NULL,
  `idaccion` int(11) NOT NULL,
  PRIMARY KEY (`idpieza`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `pieza`
--

INSERT INTO `pieza` (`idpieza`, `codigointerno`, `parte`, `idestado`, `idtipo`, `codigoexterno`, `idespecificacion`, `idusuario`, `fechaasignacion`, `fechacompra`, `idarea`, `idasignado`, `idmarca`, `idmodelo`, `idproveedor`, `idresponsable`, `numerofactura`, `disponibilidad`, `idaccion`) VALUES
(1, 'ATTa1g1', '', 1, 4, 'ATI3243893', 1, 0, '0000-00-00', '2015-02-04', 1, 0, 3, 18, 1, 0, '1', 'true', 0),
(2, 'ATTa2G2', '', 1, 4, 'ATI323d', 2, 0, '0000-00-00', '2015-02-04', 1, 0, 3, 19, 1, 0, '1', 'false', 0),
(3, 'DeMo173', '', 1, 7, 'MLCD3232', 13, 0, '0000-00-00', '2015-02-02', 1, 0, 1, 4, 1, 0, '2', 'false', 0),
(4, 'APUp754', '', 1, 6, 'APC8828734', 10, 0, '0000-00-00', '2015-03-12', 1, 0, 4, 14, 1, 0, '3', 'false', 0),
(5, 'DeTeIn5', '', 1, 8, 'ACK1223', 22, 0, '0000-00-00', '2015-01-01', 1, 0, 1, 24, 1, 0, '4', 'false', 0),
(6, 'HIHD326', '', 1, 11, 'HD88833', 19, 0, '0000-00-00', '2015-02-04', 1, 0, 6, 2, 1, 0, '1', 'false', 0),
(7, 'EPIm517', '', 1, 10, 'GHJHJ373728', 3, 0, '0000-00-00', '2015-02-04', 1, 0, 7, 26, 1, 0, '1', 'true', 0),
(8, 'EPImma8', '', 1, 10, '55446dde', 28, 0, '0000-00-00', '2015-04-01', 1, 0, 7, 26, 1, 0, '55', 'true', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor4`
--

CREATE TABLE IF NOT EXISTS `proveedor4` (
  `idproveedor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `rif` varchar(15) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `correo` varchar(75) NOT NULL,
  `direccion` text NOT NULL,
  PRIMARY KEY (`idproveedor`),
  UNIQUE KEY `rif` (`rif`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `proveedor4`
--

INSERT INTO `proveedor4` (`idproveedor`, `nombre`, `rif`, `telefono`, `correo`, `direccion`) VALUES
(1, 'newegg', '88855544622', '5558462224', 'sales@newwegg.com', 'USA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE IF NOT EXISTS `tipo` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idtipo`, `nombre`) VALUES
(13, '500va'),
(14, '750va'),
(22, 'AGP'),
(12, 'ALL IN ONE'),
(11, 'CLON LAB'),
(5, 'CRT'),
(16, 'DDR'),
(15, 'DDR2'),
(17, 'DDR3'),
(18, 'HD 4500'),
(19, 'HD 8750'),
(20, 'HDD'),
(3, 'IDE'),
(27, 'INSPIRON'),
(4, 'LCD'),
(26, 'LX3100'),
(9, 'OPTIPLEX 740'),
(8, 'OPTIPLEX 755'),
(25, 'POWEREDGE'),
(28, 'PROLIANT'),
(2, 'SATA'),
(23, 'Socket 1055 a/v/r'),
(1, 'SSD'),
(24, 'USB'),
(21, 'USB HDD'),
(10, 'VOSTRO 230');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario4`
--

CREATE TABLE IF NOT EXISTS `usuario4` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `usuario` varchar(16) NOT NULL,
  `password` varchar(25) NOT NULL,
  `tipo` int(1) NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `usuario4`
--

INSERT INTO `usuario4` (`idusuario`, `nombre`, `apellido`, `usuario`, `password`, `tipo`) VALUES
(1, 'Javierr', 'Delgado', 'admin', '19940338', 1),
(14, '', '', 'mlarosa', 'M3a2n3o0', 1),
(15, '', '', 'aquiles', 'default1', 2),
(16, '', '', 'laurab', 'bustamante', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
