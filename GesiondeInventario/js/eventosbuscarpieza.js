function eventoCargarPieza(msg){
	if(msg[0].m>0){
		console.log("Entro "+ msg);
		alerta4.innerHTML="";
		 seccioncontrol2.innerHTML='<div class="col-md-3 col-xs-3 col-sm-3"><button type="submit" class="btn btn-sm Modificar2" name="'+msg[0].idpieza+'"><span class="glyphicon glyphicon-refresh" ></span>Modificar</button></div><div class="col-md-3 col-xs-3 col-sm-3"></div><div class="col-md-3 col-xs-3 col-sm-3"></div>';
             
		CargarMarcas2(msg[0].idmarca, msg[0].idtipo);  
		cargarEspecificaciones2(msg[0].idespecificacion);
		//cargarEstados2(msg[0].idestado);
		//cargarlistadoPersonal(msg[0].idasignadopor,msg[0].idresponsablenuevo );
		//cargarlistadoAreas2(msg[0].idarea);
		cargarlistadoProveedor2(msg[0].idproveedor);
		cargarListaTipo2(msg[0].idmodelo);
		tcodigoE.value=msg[0].codigoexterno;
		tfactura2.value=msg[0].numerofactura;
		$('#tfechadeC2').val(msg[0].fechacompra); 
		$('#tfechadeA2').val(msg[0].fechaasignacion);
		tcodigoInterno2.value=msg[0].idpieza;
		obtenerImagenMarca2(msg[0].idmarca,2);
        obtenerImagenModelo2(msg[0].idtipo, 2);

		//cargarListaAccion(msg[0].idaccion);
		///cargarListaMotivo();
		//cargarlistadoPiezas();
		//cargarlistadoPiezas2();
		 $('.Modificar2').on('click',  function(){
      		var idE=$(this).attr('name');
      		console.log("click en modificar pieza"+idE);
      		$.ajax
	          ({
	            type: "POST",
	            url: "modelo/consultasEquiposyPiezas.php",
	            data: {id:20, tipo:$('#mo2').val(), marca:$('#ma2').val(), modelo:$('#tnombreEquipo2').val(),  fechaC:tfechadeC2.value, codigoexterno:tcodigoE.value, proveedor:$('#tproveedor2').val(), factura:tfactura2.value, idespecificacion:$('#tespecificacion2').val()},
	            async: false,  
	            dataType: "json",  
	            success:
	            function (msg) 
	            { 
	                 console.log(msg);
	                 if(msg=="true"){
	                    //limpiar();
	                    alerta4.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Se ha modificado correctamente la pieza!</center></div>';
	                   // coverMarca2.innerHTML="";
	                   // coverModelo2.innerHTML="";
	                 // asignarId();

	                 }
	             
	                   
	                },
	                error:
	                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
	              });


      });
		
	}
//	modificar();
}



function modificar(){
      $('.Modificar2').on('click',  function(){
      		var idE=$(this).attr('name');
      		console.log("click en modificar pieza"+idE);
      		$.ajax
          ({
            type: "POST",
            url: "modelo/consultasEquiposyPiezas.php",
            data: {id:20, tipo:$('#mo2').val(), marca:$('#ma2').val(), modelo:$('#tnombreEquipo2').val(),  fechaC:tfechadeC2.value, codigoexterno:tcodigoE.value, proveedor:$('#tproveedor2').val(), factura:tfactura2.value, idespecificacion:$('#tespecificacion2').val()},
            async: false,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                 console.log(msg);
               /*  if(msg=="true"){
                    limpiar();
                    alerta4.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Se ha registrado correctamente la pieza!</center></div>';
                    coverMarca2.innerHTML="";
                    coverModelo2.innerHTML="";
                   asignarId();

                 }*/
             
                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });

      });
}
function cargarlistadoProveedor2(p){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:7},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
            	 $('#tproveedor2').empty();           
                $('#tproveedor2').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                for (var i =0 ; i<msg[0].m; i++) {  
                   $('#tproveedor2').append('<option value="'+msg[i].idproveedor+'" >'+msg[i].nombre+'</option>');

              }
             $('#tproveedor2').val(p); 
           },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }

 function cargarListaTipo2(t){
    $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:42},
           /* async: true,  */
            dataType: "json",  
            success:
            function (msg) 
            { 
                
                $('#tnombreEquipo2').empty();           
                $('#tnombreEquipo2').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                for (var i =0 ; i<msg[0].m; i++) {                 
                      $('#tnombreEquipo2').append('<option  value="'+msg[i].idE+'" >'+msg[i].motivo+'</option>');
                }
               $('#tnombreEquipo2').val(t);

            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
  }
	 function cargarEspecificaciones2(e){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:27},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                	//console.log("Total especificaciones: "+msg[0].m);
                      $('#tespecificacion2').empty();           
                     $('#tespecificacion2').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                         $('#tespecificacion2').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');
                       //  console.log("agregando especificaciones: "+msg[i].idE);
                    }
                    $('#tespecificacion2').val(e); 

                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
    function CargarMarcas2(marca, modelo){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:17},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                   
                    $('#ma2').empty();           
                    $('#ma2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {                       
                       var path="modelo/uploads/marca/"+msg[i].idmarca+"/"+msg[i].imagen;
                        /*$('#ma').append('<option data-img-src="'+path+'" value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');
                        $('#ma2').append('<option data-img-src="'+path+'" value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');*/
                        $('#ma2').append('<option  value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');

                        
                  }   
                   $('#ma2').val(marca);
                    cargarModelos2(modelo);   
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }


    function cargarModelos2(modelo){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:21},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                     $('#mo2').empty();           
                     $('#mo2').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');

                    for (var i =0 ; i<msg[0].m; i++) { 

                      if(msg[i].idmodelo2>3)     {
                           $('#mo2').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');                    
                      		// console.log("Agregando el id tipo: "+msg[i].idmodelo2+ " al select");
                      }            
                     }  

                     $('#mo2').val(modelo) ;
                   //  console.log("Seleccionar el tipo numero: "+modelo);    

                           
                       /* $(".my-select").chosen({
                            width:"100%",
                          });*/
                         $( "#ma2" ).change(function (e) {
                             e.preventDefault();
                             $("#cargaMarca2").css("display", "block");
                             marca=$('#ma2').val();
                             obtenerImagenMarca2(marca, 2);
                             console.log("click en marca: "+marca);

                         });
                         $( "#mo2" ).change(function (e) {        
                            /* console.log($('#mo').val());*/
                            e.preventDefault();
                             $("#cargaModelo2").css("display", "block");
                             modelo=$('#mo2').val();
                             console.log("tipo seleccionado:" +modelo);
                             obtenerImagenModelo2(modelo, 2);
                         });  
                        

                    
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

      function obtenerImagenModelo2(modelo, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:22, idmodelo:modelo },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {             
                    
                    if(ban==1){
                      $('#coverModelo2').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo2.innerHTML='<img src="'+path+ '"  width="110" height="110" />'; 
                       $("#cargaModelo2").css("display", "none");

                    } 
                    if(ban==2){
                      $('#coverModelo2').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo2.innerHTML='<img src="'+path+ '"  width="110" height="110" />'; 
                       $("#cargaModelo2").css("display", "none");
                    }     

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

      function obtenerImagenMarca2(marca, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:18, idmarca:marca },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                  
                    if(ban==1){
                      $('#coverMarca2').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca2.innerHTML='<img src="'+path+ '"  width="110" height="110" />'; 
                       $("#cargaMarca2").css("display", "none");

                    } 
                    if(ban==2){
                      $('#coverMarca2').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca2.innerHTML='<img src="'+path+ '"  width="110" height="110" />'; 
                       $("#cargaMarca2").css("display", "none");
                    }

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }