 <div class="row">
        
                <div class="col-md-12 col-xs-12 col-sm-12">                
                     <div class="form-group">
                            <label class="col-sm-1 control-label">Accion</label>
                          <div class="col-sm-3">                              
                              <select class="form-control input-sm" name="filtro" id="tfiltro">
                                    <option value="0"disabled selected> --Seleccione Filtro -- </option>
                                    <option value="1" >Creado</option>
                                    <option value="2" >Modificado</option>
                                    <option value="3" >Eliminado</option>
                                    <option value="4" >Fecha </option>                                    
                              </select> 
                          </div>

                      <div class="form-group">
                            <label class="col-sm-1 control-label">Usuario</label>
                          <div class="col-sm-3">                              
                              <select class="form-control input-sm" name="filtrousuario" id="tfiltrousuario">
                                    <!-- <option value="0"disabled selected> --Seleccione Filtro -- </option> -->
                                                                    
                              </select> 
                          </div>
                      </div>

                      <div class="form-group">
                            <label class="col-sm-1 control-label">Area:</label>
                          <div class="col-sm-3">                              
                              <select class="form-control input-sm" name="filtro2" id="tfiltro2">
                                    <option value="0"disabled selected> --Seleccione Filtro -- </option>
                                   
                          </div>
                      </div>

                      <div class="form-group">
                            <label class="col-sm-1 control-label">Especificacion</label>
                          <div class="col-sm-3">                              
                              <select class="form-control input-sm" name="filtro4" id="tfiltro4">
                                  
                              </select> 
                          </div>
                      </div>
                       <div class="form-group">
                            <label class="col-sm-1 control-label">Tipo:</label>
                          <div class="col-sm-3">                              
                              <select class="form-control input-sm" name="filtro3" id="tfiltro3">
                                   
                              </select> 
                          </div>
                      </div>


                          

                         
                          <div class="col-sm-1">
                                 Asc <input  id="asc" type="radio" class="form-control input-sm" >
                          </div>
                          <div class="col-sm-1">
                            Desc <input  id="desc" type="radio" class="form-control input-sm" checked>
                          </div>

                          <div class="col-sm-1">
                                    <button type="submit" class="btn" id="ver" ><span class="glyphicon glyphicon-search" ></span>Ver</button>
                          </div>

                      
                </div>
            </div>
            <div id="resultadonumero"></div>
            <section id="listado">
                
            </section>


            <hr><br><br>
           <section>
            <div class="row">
        		
        		<div class="col-md-2 col-xs-2 col-sm-2">
                        <button type="submit" class="btn" id="exportar" style="display:none;" title="Exportar"><span class="glyphicon glyphicon-file" ></span></button>
                        <button type="submit" class="btn" id="imprimir" style="display:none;" title="Imprimir"><span class="glyphicon glyphicon-print" ></span></button>

        		</div>
                <div class="col-md-6 col-xs-6 col-sm-6">  
	           		<div class="form-group" style="display:none;" id="selectequipo">
	                            <label class="col-sm-2 control-label">Seleccione para imprimir</label>
	                          <div class="col-sm-4">                              
	                              <select class="form-control input-sm" multiple="multiple" id="tlistequipos">                            
	                                                                    
	                              </select> 
	                          </div>
	                      </div>
	                  </div>
              </div>

           </section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Acciones</h4>
      </div>
      <div class="modal-body" id="acciones">
       
      </div>
      <div class="modal-footer" id="boton">
        
        

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $('#menuprincipal').on('click',  function(){
    /*$("#contenedor").hide().load('partials/menu/menuadministrador.html', function(){      
    }).fadeIn(1500);*/
        window.open('index.php' , '_self');
        
  });
    var ban=2;
    cargarUSuariosHistorialUsuarios();
    cargarlistadoAreas();
    cargarModelos();
    cargarListaTipo();
  //  cargarEspecificaciones();
  	/*$('#exportar').click(function(){
    	window.print();
    	return false;
	});*/
    function cargarUSuariosHistorialUsuarios(){

         $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:7},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                      /*  console.log(msg);*/
                       for (var i =0 ; i<msg[0].m; i++) {                      
                        
                        $('#tfiltrousuario').append('<option  value="'+msg[i].usuario+'" >'+msg[i].usuario+'</option>');

                        
                  }   
                         
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          }); 
    }

     $('#imprimir').on('click',  function(){
        if(!$('#tfiltro2').val()==false){
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tlistequipos').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         /*console.log(msg);*/
                         window.open("modelo/imprimirreporteporArea.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });   
        }
          if(!$('#tfiltro3').val()==false){
                         console.log("entro");

             window.open("modelo/imprimirreporteporTipo.php");
          }
          if(!$('#tfiltrousuario').val()==false){
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tfiltrousuario').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
/*                         console.log(msg);
*/                         window.open("modelo/imprimirreporteporUsuario.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });   
            }
            if(!$('#tfiltro').val()==false){
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tfiltro').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         /*console.log(msg);*/
                         window.open("modelo/imprimirreporteporAccion.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });   
            }
     });
     $('#exportar').on('click',  function(){
       /*   if(!$('#tlistequipos').val()==false){
                       $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tlistequipos').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         console.log(msg);
                         window.open("modelo/listado.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });   
          }*/
          if(!$('#tfiltrousuario').val()==false&&!$('#tlistequipos').val()==false){
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tlistequipos').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         console.log(msg);
                         window.open("modelo/listadousuarios.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });   

          }
          if(!$('#tfiltro2').val()==false&&!$('#tlistequipos').val()==false){
            console.log("Entro" +$('#tlistequipos').val() );

                $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tlistequipos').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         console.log(msg);
                         window.open("modelo/listadoporaccion.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
          }
          if(!$('#tfiltro3').val()==false&&!$('#tlistequipos').val()==false){
               $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:$('#tlistequipos').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         console.log(msg);
                         window.open("modelo/listadoportipo.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
          }
     });

    $('#ver').on('click',  function(){
        if(!$('#tfiltro').val()==false){
            buscar();
            $('#selectequipo').fadeOut();
            $('#exportar').fadeOut();

        }
        if(!$('#tfiltrousuario').val()==false){
            buscar2();
            $('#exportar').fadeOut();
            $('#selectequipo').fadeOut();

        }
         if(!$('#tfiltro2').val()==false){
            $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:10, data:$('#tfiltro2').val(), ban:ban},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                   console.log(msg);
                    if(msg[0].m>0){
                      $('#selectequipo').fadeIn();
                      $('#exportar').fadeIn();
                      $('#imprimir').fadeIn();



                    }
                    else{
                      $('#selectequipo').fadeOut();
                      $('#exportar').fadeOut();
                      $('#imprimir').fadeOut();


                    }


                   
                           
                           $('#listado').html("");                        
                            var table = $('<table style="width:100%;" class="table" ></table>');                         
                            var row=$('<thead></thead>').append("<tr></tr>")
                            $('#tlistequipos').empty();
                            for (var i =0; i<msg[0].m; i++) {
                              $('#tlistequipos').append('<option value="'+msg[i].codigointerno+'" >'+msg[i].codigointerno+'</option>');
                            }
                            $('#listado').append(listarE(msg, table, row));
                           // editar();   
                                 
                   
                                               
                       
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
        }
         if(!$('#tfiltro3').val()==false){
            $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:11, data:$('#tfiltro3').val(), ban:ban},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                   console.log(msg);
                    if(msg[0].m>0){
                     $('#selectequipo').fadeIn();
                      $('#exportar').fadeIn();
                      $('#imprimir').fadeIn();


                    }
                    else{
                      $('#selectequipo').fadeOut();
                      $('#exportar').fadeOut();
                      $('#imprimir').fadeOut();


                    }


                  
                           
                           $('#listado').html("");                        
                            var table = $('<table style="width:100%;" class="table" ></table>');                         
                            var row=$('<thead></thead>').append("<tr></tr>")
                            $('#listado').append(listarE(msg, table, row));
                           // editar();   
                                 
                              $('#tlistequipos').empty();
                            for (var i =0; i<msg[0].m; i++) {
                              $('#tlistequipos').append('<option value="'+msg[i].codigointerno+'" >'+msg[i].codigointerno+'</option>');
                            }
                           // editar();   
                                               
                       
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
         }
    });

    $("#asc").change(function () {
                $("#desc").attr('checked', false);
                ban=1;
            });
	 $("#desc").change(function () {
	    $("#asc").attr('checked', false);
	    ban=2;                
	});          


    $('#tfiltrousuario').change(function () {
       $('#tfiltro').val(0);
        $('#tfiltro2').val(0);
        $('#tfiltro3').val(0);
        /*$('#tfiltro4').val(0);*/
        // $('#exportar').fadeOut();
        // buscar2();
       
    });

    $('#tfiltro').change(function () {
        $('#tfiltrousuario').val(0);
        $('#tfiltro2').val(0);
        $('#tfiltro3').val(0);
       /* $('#tfiltro4').val(0);*/
        // $('#exportar').fadeOut();
         //buscar();

    });
     $('#tfiltro2').change(function () {
        $('#tfiltrousuario').val(0);
        $('#tfiltro').val(0);
        $('#tfiltro3').val(0);
        /*$('#tfiltro4').val(0);*/

    });
     $('#tfiltro3').change(function () {
        $('#tfiltrousuario').val(0);
        $('#tfiltro').val(0);
        $('#tfiltro2').val(0);       
        /*$('#tfiltro4').val(0);*/
       //  $('#exportar').fadeOut();
    });
     $('#tfiltro4').change(function () {
        $('#tfiltrousuario').val(0);
        $('#tfiltro').val(0);
        $('#tfiltro2').val(0);
        $('#tfiltro3').val(0);
       //  $('#exportar').fadeOut();


    });

       function cargarListaTipo(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:21},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    
                        
                      $('#tfiltro3').empty();           
                     $('#tfiltro3').append(' <option value="0" disabled selected> --Seleccione un filtro -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                         if(msg[i].idmodelo2==1||msg[i].idmodelo2==2||msg[i].idmodelo2==3) 
                          $('#tfiltro3').append('<option value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');
                    }
                    
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

         function cargarEspecificaciones(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:27},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                      $('#tfiltro4').empty();           
                     $('#tfiltro4').append(' <option value="0" disabled selected> --Seleccione un filtro -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                         $('#tfiltro4').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');
                    }
                   

                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

       function cargarlistadoAreas(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:12},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                      console.log(msg);
                      $('#tfiltro2').empty();           
                     $('#tfiltro2').append(' <option value="0" disabled selected> --Seleccione un filtro -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                         $('#tfiltro2').append('<option value="'+msg[i].idareas+'" >'+msg[i].nombre+'</option>');
                    }
                   

                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });


      
    }
    function buscar3(){}

        function cargarModelos(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:21},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    $('#tfiltro3').empty();           
                     $('#tfiltro3').append(' <option value="0" disabled selected> --Seleccione un filtro--</option>');

                    for (var i =0 ; i<msg[0].m; i++) {   
                      if(msg[i].idmodelo2==1||msg[i].idmodelo2==2||msg[i].idmodelo2==3)                 
                           $('#tfiltro3').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');                    
                     }              
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
    function buscar(){

        console.log("ban: "+ban+" valor: "+$('#tfiltro').val());
        $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:5, filtro:$('#tfiltro').val(), ban:ban},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                      console.log(msg);
                        $('#listado').html("");
                       if(msg[0].m>0){
                            $('#imprimir').fadeIn();

                        }
                        else{
                          $('#imprimir').fadeOut();

                        }
                        var table = $('<table style="width:100%;" class="table"></table>');                         
                        var row=$('<thead></thead>').append("<tr></tr>")
                         $('#tlistequipos').empty();
                            for (var i =0; i<msg[0].m; i++) {
                               /* console.log("agregando"+ msg[i].idhistorialusuario);*/
                             // $('#tlistequipos').append('<option value="'+msg[i].codigointerno+'" >'+msg[i].codigointerno+'</option>');
                               $('#tlistequipos').append('<option  value="'+msg[i].idhistorialusuario+'" >'+msg[i].idhistorialusuario+'</option>');
                              
                            }
                        $('#listado').append(ListarTabla(msg, table, row));  
                         editar();

                         
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          }); 
    }
        function buscar2(){

        console.log("ban: "+ban+" usuario: "+  $('#tfiltrousuario').val());
        $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:8, filtro:  $('#tfiltrousuario').val(), ban:ban},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                		console.log(msg);
                		//$('#selectequipo').fadeOut();
                		//	$('#exportar').fadeOut();
                		if(msg[0].m>0){
                			$('#imprimir').fadeIn();

                		}
                		else{
                			$('#imprimir').fadeOut();

                		}


                     $('#tlistequipos').empty();
                      for (var i =0; i<msg[0].m; i++) {
                          console.log("agregando"+ msg[i].idhistorialusuario);
                       // $('#tlistequipos').append('<option value="'+msg[i].codigointerno+'" >'+msg[i].codigointerno+'</option>');
                         $('#tlistequipos').append('<option  value="'+msg[i].idhistorialusuario+'" >'+msg[i].idhistorialusuario+'</option>');
                        
                      }
                        $('#listado').html("");
                        
                        var table = $('<table style="width:100%;" class="table"></table>');                         
                        var row=$('<thead></thead>').append("<tr></tr>")
                        $('#listado').append(ListarTabla(msg, table, row));  
                         editar();
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          }); 
    }

function imprimir2(){
  $('#imprimir2').on('click',  function(){
          var idE=$(this).attr('name');
        console.log(idE);

        $.ajax
              ({
                type: "POST",
                url: "modelo/consultasreporte.php",
                data: {id:12, ids:idE},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                        
                         console.log(msg);
                         window.open("modelo/listadodeacciones.php");

                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
  });
}
function editar(){
	console.log();
		$('.editar').on('click',  function(){
      		var idE=$(this).attr('name');
      		$.ajax
		        ({
		          type: "POST",
		          url: "modelo/consultasreporte.php",
		          data: {id:6, ban:idE},
		          async: true,  
		          dataType: "json",  
		          success:
		          function (msg) 
			        { 
                $('#acciones').html("");


			        	if(msg[0].d>0){
                 // console.log("total acciones: "+msg[0].d);
                 boton.innerHTML='<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><a type="submit" class="btn" id="imprimir2" title="Imprimir" name="'+idE+'"><span class="glyphicon glyphicon-print" ></span>Imprimir</a>';
                    imprimir2();
					         for (var i = 0; i<msg[0].d; i++) {
					            $('#acciones').append("Se hizo la Accion: <b>"+msg[i].accion+ "</b> por el Usuario:  <b>"+ msg[i].usuario + "</b> el dia : <b>"+msg[i].fecha+ "</b> a la Hora: <b>"+msg[i].hora+"</b><br>");
					           //console.log("agrego accion "+i);
                     //setTimeout(function(){ alert("Hello"); }, 3000);
                   }
			        	
			        		
			        	}
			        	else{
			        	 $('#acciones').html("<center>Este Equipo ya no existe fue eliminado</center>");

			        	}
				
			        },
		          error:
		          function (msg) {console.log( msg +" No se pudo realizar la conexion");}
		        });
      	});
 }

    function ListarTabla(msg, table, row){         
                row.append($('<th></th>').html("IDS"));
                row.append($('<th></th>').html("<b>ACCION</b>"));
                row.append($('<th></th>').html("<b>CODIGO INTERNO</b>"));
                row.append($('<th></th>').html("<b>EJECUTADO POR </b>"));
                row.append($('<th></th>').html("<b>FECHA</b>"));
                row.append($('<th></th>').html("<b>HORA</b>"));
                table.append(row);  
                var row2 = $('<tbody></tbody>');
                resultadonumero.innerHTML="Mostrando <span class='badge'>"+msg[0].m+"</span> Resultados";
                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr></tr>');
                        var fila0=$('<td></td>').text((msg[i].idhistorialusuario));
                        var fila2 = $('<td></td>').text(msg[i].accion);
                        var fila3 = $('<td></td>').html('<a  href="#" class="editar" data-toggle="modal" data-target="#myModal"  title="Ver Acciones" name="'+msg[i].idE+'"> '+msg[i].idE+'</a>');
                        var fila4 = $('<td></td>').text(msg[i].ejecuta);
                        var fila5 = $('<td></td>').text(msg[i].fecha);
                        var fila6 = $('<td></td>').text(msg[i].hora);
                        // $('#tlistequipos').empty();


                                           
                        

                         row3.append(fila0);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);  
                        row3.append(fila5);
                        row3.append(fila6);

                        row2.append(row3);                              
                       
                    
                     
                }
                table.append(row2);
                return table;
            }



     function listarE(msg, table, row){
            row.append($('<th></th>').html("Codigo Interno"));
                row.append($('<th></th>').html("<b>Equipo</b>"));                            
                row.append($('<th></th>').html("<b>Marca</b>"));
                row.append($('<th></th>').html("<b>Modelo</b>"));
                row.append($('<th></th>').html("<b>Especificacion</b>"));  
                table.append(row);  
                var row2 = $('<tbody></tbody>');
                resultadonumero.innerHTML="Mostrando <span class='badge'>"+msg[0].m+"</span> Resultados";

                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr id="'+msg[i].idequipo+'"></tr>');
                        var fila0=$('<td></td>').text(msg[i].codigointerno);
                        var fila1 = $('<td></td>').text(msg[i].nombretipo);
                        var fila2 = $('<td></td>').text(msg[i].nombremarca);
                        var fila3 = $('<td></td>').text(msg[i].nombremodelo);
                        var fila4 = $('<td></td>').text(msg[i].especificacion);

                        row3.append(fila0);
                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);
                        row2.append(row3);             
                }
                table.append(row2);
                return table;
      }    



