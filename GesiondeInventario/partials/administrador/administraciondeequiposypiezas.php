<?php
  session_start();
 
?>
    <link rel="stylesheet" href="css/chosen.css">
    <link rel="stylesheet" href="css/ImageSelect.css">

<div class="row">
        <div class="col-xs-12">
             <ol class="breadcrumb">
                <li id="menuprincipal"><a href="#">Menu Principal</a>
                </li>
                <li class="active">Administracion de equipos y piezas</li>
            </ol>
        </div>
</div>

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
  <?php  if($_SESSION['tipo']==3) { ?>
    <script type="text/javascript">
    $("#espera").css("display", "block");
        $("#home").hide().load('partials/administrador/equiposypiezas/modificar.php', function(){
              $("#espera").css("display", "none");

        }).fadeIn(1500);
</script>
    <li role="presentation" class="active" id="modificar2"><a href="#modificar" aria-controls="modificar" role="tab" data-toggle="tab">Buscar</a></li>
<?php } else {?>
    <script type="text/javascript">
    $("#espera").css("display", "block");
    $("#home").hide().load('partials/administrador/equiposypiezas/agregar.php', function(){
          $("#espera").css("display", "none");

    }).fadeIn(1500);
</script>
    <li role="presentation" class="active" id="home2"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Agregar</a></li>
 
    <li role="presentation"  id="modificar2"><a href="#modificar" aria-controls="modificar" role="tab" data-toggle="tab">Buscar</a></li>

    <?php } ?>



  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      <div id="espera" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>

    <div role="tabpanel" class="tab-pane active" id="home">




      </div><!-- Tabpane -->

    <div role="tabpanel" class="tab-pane" id="modificar">
    </div>


  </div>

</div>












 <script type="text/javascript">
  $(document).ready(function() {
/*    $("#espera").css("display", "block");
    $("#home").hide().load('partials/administrador/equiposypiezas/agregar.php', function(){
          $("#espera").css("display", "none");

    }).fadeIn(1500);*/

  	$('#menuprincipal').on('click',  function(){  window.open('index.php' , '_self');});
    $('#home2').on('click',  function(e){
        e.preventDefault();
             $("#espera").css("display", "block");
        $("#home").hide().load('partials/administrador/equiposypiezas/agregar.php', function(){
              $("#espera").css("display", "none");

        }).fadeIn(1500);
    });
     $('#modificar2').on('click',  function(e){
        e.preventDefault();
             $("#espera").css("display", "block");
        $("#home").hide().load('partials/administrador/equiposypiezas/modificar.php', function(){
              $("#espera").css("display", "none");

        }).fadeIn(1500);
    });
      $('#eliminar2').on('click',  function(e){
        e.preventDefault();
             $("#espera").css("display", "block");
        $("#home").hide().load('partials/administrador/equiposypiezas/eliminar.html', function(){
              $("#espera").css("display", "none");

        }).fadeIn(1500);
    });

 });


</script>