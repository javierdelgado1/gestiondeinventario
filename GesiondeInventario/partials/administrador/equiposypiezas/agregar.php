         <?php date_default_timezone_set('America/Caracas'); ?>

 <?php
  session_start();
 
?>


       <?php  if($_SESSION['tipo']!=3) { ?>  
         <section >
            <div id="alertas"> 
                <div class="alert alert-info">
                        <center>
                                <h6><b>Registrar Equipo o Pieza</b></h6>                      
                        </center>
                </div>
            </div>
        </section>


    <form id="formregistrar"  method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">  
                     <div class="form-group">
                         <label class="col-sm-4 control-label">Agregar Nuevo</label>
                          <div class="col-sm-8">
                              <select  class="form-control input-sm"  name="tipo"  id="ttipo"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                    <option value="1">Equipo</option>
                                    <option value="2">Pieza</option>
                               </select> 
                          </div>
                      </div>
                </div>
            </div>
            <hr></form>
    <?php }  ?>
<section id="Equipo" style="display:none;">
<div class="panel panel-default">
  <div class="panel-heading"> <b><h4>Agregar Equipo</h4></b></div>
  <div class="panel-body">    
  
    <form id="formregistrar2"  method="post" class="form-signin form-horizontal"  onsubmit="return false;">  

             <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Tipo:</label>
                          <div class="col-sm-6"  style="padding-right:0px;"> 
                              <select  id="mo" class="form-control input-sm" name="modelo">
                                    <option value="0"  disabled selected> --Seleccione una opcion -- </option>                                  
                                </select> 
                         
                        </div>
                         <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaModelo" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarModelo" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>


                      </div>
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Marca:</label>
                          <div class="col-sm-6" style="padding-right:0px;">                             
                               <select  id="ma" class="form-control input-sm" name="marca">
                                    <option value="0" disabled selected> --Seleccione una opcion -- </option>
                                </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaMarca" title="Crear Nueva Marca"><span class="glyphicon glyphicon-plus" ></span></button>
                          </div>
                          
                          <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarMarca" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>

                      </div>

                      <div class="form-group">
                         <label class="col-sm-4 control-label">Modelo:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  id="tnombreEquipo" class="form-control input-sm" name="nombreEquipo">
                                    <option value="0" disabled selected> --Seleccione una opcion -- </option>             

                              </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaModeloD" title="Crear Nueva Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                          </div>
                          
                          <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarModeloD" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>

                       
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Especificacion:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="especificacion"  id="tespecificacion"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaEspecificacion" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarEspecificacion" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>

                      </div>
                       <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Estado:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="estado"  id="testado" disabled="disabled">
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <!--  <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaEstado" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarEstado" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div> -->
                      </div>
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Fecha de Compra:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tfechadeC" type="date" class="form-control  input-sm" name="fechadeC" max=<?php  echo date("Y-m-d"); ?>  >
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>

                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Area:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="area"  id="tArea"  disabled="true">
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
<!--                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaArea" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarArea" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div> -->
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Externo:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                             <input  id="tcodigoexterno" type="text" class="form-control input-sm" name="codigoexterno" >  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Proveedor:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="proveedor"  id="tproveedor"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaProveedor" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarProveedor" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Nª de Factura:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                             <input  id="tfactura" type="text" class="form-control input-sm" name="emfactura" >  
                             
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      </div>

                <div class="col-md-1 col-xs-1 col-sm-1"> 
                </div>

                <div class="col-md-5 col-xs-5 col-sm-5"> 
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Interno:</label>
                           <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tcodigoInterno" type="text" class="form-control input-sm" name="codigoInterno" disabled="true">  
                              
                          </div>
                      </div>
                       <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Marca</small></div>                     
                            <div class="panel-body">
                                 <div id="cargaMarca" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverMarca"></div>
                                  
                                </center>
                             </div>
                      </div>
                      <br>
                       <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Tipo</small></div>                     
                            <div class="panel-body">
                                <div id="cargaModelo" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverModelo"></div>
                                  
                                </center>
                             </div>
                      </div>

                      
                </div>
            </div><hr>

            <section id="piezas3">              
              <div role="tabpanel">        
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active" id="piezas2"><a href="#piezas" aria-controls="piezas" role="tab" data-toggle="tab">Piezas</a></li>
                  <!-- <li role="presentation" id="historial2"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Historial</a></li> -->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="piezas">
                    
                        
                           

                                    <div class="form-group">
                                       <label class="col-sm-3 control-label">Seleccione un Componente:</label>
                                        <div class="col-sm-6">
                                             <select  class="form-control input-sm"  name="priority" multiple="multiple" id="sPiezas"></select>                                           
                                        </div>
                                    </div>
                              

                           <div class="row">                                
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                      <div class="panel panel-default">
                                        <div class="panel-heading"> <small>Seleccionados</small></div>
                                         <div class="panel-body" id="listadopiezas">

                                           </div>
                                      </div>
                                </div>
                            </div>

                    <div id="historial">
                    
                    </div>

                  </div>
                  
                </div>
            </section>
                 <center>
                     <div class="row">                        
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                    <button type="submit" class="btn btn-sm btn-primary" id="guardar" disabled="true"><span class="glyphicon glyphicon-floppy-disk" ></span>Registrar</button>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                              <section id="alerta3"></section>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                    <button type="button" class="btn btn-sm btn-danger" id="cancelar"><span class="glyphicon glyphicon-trash"></span>Cancelar</button>                        
                            </div>                      
                      </div>          
                </center>
            </form>

          </div>
        </div>
      </section>

  <section id="Pieza" style="display:none;">
    <div class="panel panel-default">
      <div class="panel-heading"> <b><h4>Agregar Pieza</h4></b></div>
      <div class="panel-body">
        
           
            <form id="formregistrar3"  method="post" class="form-horizontal"  onsubmit="return false;"> 
                <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                  <div class="form-group">
                         <label class="col-sm-4 control-label">Tipo:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  id="mo2" class="my-select form-control input-sm" name="modelo2">
                                    <option value="0"  disabled selected> --Seleccione una opcion -- </option>                                  
                                </select>            
                                
                        </div>
                         <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaModelo2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarModelo2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>

                      </div>
                    
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Marca:</label>
                          <div class="col-sm-6" style="padding-right:0px;">                             
                               <select  id="ma2" class="my-select form-control input-sm" name="marca2">
                                    <option value="0" > --Seleccione una opcion -- </option>
                                </select> 
                                
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaMarca2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarMarca2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Modelo </label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  id="tnombreEquipo2" class="form-control input-sm" name="nombreEquipo2">
                                    <option value="0" > --Seleccione una opcion -- </option>                                

                              </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaTipo" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarTipo" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Especificacion:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="especificacion2"  id="tespecificacion2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaEspecificacion2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarEspecificacion2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Estado:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="estado2"  id="testado2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaEstado" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarEstado" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Fecha de Compra:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tfechadeC2" type="date" class="form-control input-sm" name="fechadeC2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>

                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Fecha de Asignacion :</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tfechadeA2" type="date" class="form-control input-sm" name="fechadeA2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                     <!--  -->
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Asignado por:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="asignadopor2"  id="tasignadopor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div> 
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Nuevo Responsable:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="nuevoresp2"  id="tnuevoresp2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Area:</label>
                         <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="area2"  id="tArea2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Externo:</label>
                          <div class="col-sm-6" style="padding-right:0px;">  
                              <input  id="tcodigoE" type="text" class="form-control input-sm" name="codigoE">                        
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Proveedor:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="proveedor2"  id="tproveedor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-xs" id="nuevaProveedor2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-xs" id="actualizarProveedor2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Nª de Factura:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                             <input  id="tfactura2" type="text" class="form-control input-sm" name="emfactura2">  
                             
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      
                </div>
                <div class="col-md-1 col-xs-1 col-sm-1"> 
                </div>

                <div class="col-md-5 col-xs-5 col-sm-5"> 
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Interno:</label>
                          <div class="col-sm-6">
                              <input  id="tcodigoInterno2" type="text" class="form-control input-sm" name="codigoInterno2" disabled="true">  
                              
                          </div>
                      </div>
                         <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Marca</small></div>                     
                            <div class="panel-body">
                                 <div id="cargaMarca2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverMarca2"></div>
                                  
                                </center>
                             </div>
                      </div>
                      <br>
                       <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Tipo</small></div>                     
                            <div class="panel-body">
                                <div id="cargaModelo2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverModelo2"></div>
                                  
                                </center>
                             </div>
                      </div>
                     
                </div>
            </div>

            <hr>
                  <center>
                     <div class="row">                        
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                     <button type="submit" class="btn btn-primary" id="guardar2"  disabled="disabled"><span class="glyphicon glyphicon-floppy-disk" ></span>Registrar</button>                      
                                   
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                              <section id="alerta4"></section>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                    <button type="button" class="btn btn-danger" id="cancelar2"><span class="glyphicon glyphicon-trash"></span>Cancelar</button>  
                                                          
                            </div>                      
                      </div>          
                </center>                  
            </form>
      </div>
    </div>
  </section>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Nueva</h4>
      </div>
      <div class="modal-body" id="personal">
     
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Nuevo</h4>
      </div>
      <div class="modal-body" id="personal2">
     
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>


 <script type="text/javascript">
  $(document).ready(function() {
  $('#menuprincipal').on('click',  function(){  window.open('index.php' , '_self');});
    var marca="-1", modelo="-1",  marca2="-1", modelo2="-1", banpiezas="-1";
    var ma="", ma2="", mo="", mo2="", es="", es2="", ID="", ID2=""; 


    $('.date-pick').datePicker(
    {
      startDate: '01/01/1970',
      endDate: (new Date()).asString()
    }
    );
/*    $('.date').datepicker({
                    language: "es",
                    todayHighlight: true,
                    format: "dd/mm/yyyy"
                });*/  
    $('.date').on
    $('#formregistrar2').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                nombreEquipo:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                  marca:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                modelo:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                especificacion:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                estado:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                especificacion:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeC:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeA:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                asignadopor:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }
                ,
                nuevoresp:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                area:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                codigoexterno:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                proveedor:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                emfactura:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             digits: {
                                   message: 'Por favor introduce sólo dígitos'
                              },
                              stringLength: {
                                   max: 9,
                                  message: 'Por favor introduce menos de %s caracteres'
                              }  
                      }
                }
            }
              
          })
            .on('success.field.fv', function(e, data) {  
        /*    data.fv.revalidateField('fechadeC') ;
            data.fv.revalidateField('fechadeA') ;*/
           


               if (!data.fv.isValid()) {    // There is invalid field
                     data.fv.disableSubmitButtons(true);  
                     /* data.fv.revalidateField('fechadeC');
                       data.fv.revalidateField('fechadeA'); */  
                    
                  }
            });


$('#formregistrar3').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                  nombreEquipo2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                  marca2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                modelo2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                especificacion2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }/*,
                estado2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }*/,
                especificacion2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeC2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeA2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
               /* asignadopor2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }
                ,*/
                /*nuevoresp2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },*/
                /*area2:{
                    validators2: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },*/
                
                proveedor2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                emfactura2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             digits: {
                                   message: 'Por favor introduce sólo dígitos'
                              },
                              stringLength: {
                                   max: 9,
                                  message: 'Por favor introduce menos de %s caracteres'
                              }  
                      }
                },
                 /* cantidad:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             digits: {
                                    message: 'Por favor introduce sólo dígitos'
                                    }
                          }
                },*/
                /*parte:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                     }
                },*/
               
                codigoE:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }
            }
              
          })
            .on('success.field.fv', function(e, data) {   
               if (!data.fv.isValid()) {    // There is invalid field
                     data.fv.disableSubmitButtons(true);     
                    
                  }
            });
 
    CargarMarcas();    
    cargarEspecificaciones();
    cargarEstados();
    cargarlistadoPersonal();
    cargarlistadoAreas();
    cargarlistadoProveedor();
    cargarlistadoPiezas();
    asignarId();
    cargarListaTipo();
    cargarModelos();
     

var table = $('<table style="width:100%;" class="table" ></table>');                         
var row=$('<thead></thead>').append("<tr></tr>");
row.append($('<th></th>').html(""));
/*row.append($('<th></th>').html("<b>Cantidad</b>")); */                           
row.append($('<th></th>').html("<b>Tipo</b>"));
row.append($('<th></th>').html("<b>Estado</b>"));
row.append($('<th></th>').html("<b>Codigo Externo</b>"));
row.append($('<th></th>').html("<b>Especificacion</b>"));
row.append($('<th></th>').html("<b>Marca</b>"));
row.append($('<th></th>').html("<b>Modelo</b>"));

var row2 = $('<tbody id="cuerpotabla"></tbody>');
table.append(row2);


$('#listadopiezas').append(table);



table.append(row);  
    $('#ttipo').change(function () {
     /* console.log("click: "+$('#ttipo').val());*/
        if($('#ttipo').val()==1){
          $('#Equipo').fadeIn();
          $('#Pieza').fadeOut();

        }
        else{
          $('#Equipo').fadeOut();
          $('#Pieza').fadeIn();


        }

    });

      $('#sPiezas').change(function () {
          if(!$('#sPiezas').val()==false){
             banpiezas=$('#sPiezas').val();
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasEquiposyPiezas.php",
                data: {id:3, ids:$('#sPiezas').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                    
                    $('#cuerpotabla').html("");              
                    var row3="";
                    console.log("tamano: "+msg[0].m);
                    if(msg[0].m==0){
                      alert("Escoja otra pieza, lo que selecciono ya no esta disponible");
                    }
                    for (var i = 0; i <msg[0].m; i++) {
                        console.log(i);
                        var row3=$('<tr id="'+msg[i].idpieza+'"></tr>');
                     
                        var fila0=$('<td></td>').text(i);
                      /*  var fila1 = $('<td></td>').text(msg[i].cantidad);*/
                        var fila2 = $('<td></td>').text(msg[i].parte);
                        var fila3 = $('<td></td>').text(msg[i].estado);
                        var fila4 = $('<td></td>').text(msg[i].codigoexterno);
                        var fila5 = $('<td></td>').text(msg[i].especificacion);
                        var fila6 = $('<td></td>').text(msg[i].nombremarca);
                        var fila7 = $('<td></td>').text(msg[i].nombremodelo);

                         row3.append(fila0);
                       /* row3.append(fila1);*/
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);
                        row3.append(fila5);
                        row3.append(fila6);
                        row3.append(fila7);

                      $('#cuerpotabla').append(row3); 
                }            
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
          }
          else{
                $('#cuerpotabla').html("");
                banpiezas="-1";
          }


      });
 $('#actualizarMarca').on('click',  function(){
    CargarMarcas();
 });
  $('#actualizarModelo').on('click',  function(){
    cargarModelos();
 });
  $('#actualizarModeloD').on('click',  function(){
    cargarListaTipo();
 });

$('#actualizarEspecificacion').on('click',  function(){
  cargarEspecificaciones();
 });

$('#actualizarProveedor').on('click',  function(){
  cargarlistadoProveedor();
 });


 $('#actualizarMarca2').on('click',  function(){
    CargarMarcas();
 });
  $('#actualizarModelo2').on('click',  function(){
    cargarModelos();
 });

$('#actualizarEspecificacion2').on('click',  function(){
  cargarEspecificaciones();
 });

$('#actualizarProveedor2').on('click',  function(){
  cargarlistadoProveedor();
 });

$('#actualizarTipo').on('click',  function(){
  cargarListaTipo();
 });


    $('#guardar').on('click',  function(){
        console.log($("#ma :selected").text());
        if(validar()){
        var   pieza=$('#sPiezas').val() ;
        console.log("Piezas Seleccionadas: " +!$('#sPiezas').val());
        if(!$('#sPiezas').val())
          pieza=-1;
          $.ajax
          ({
            type: "POST",
            url: "modelo/consultasEquiposyPiezas.php",
            data: {id:5,  codigointerno:tcodigoInterno.value, nombre:$('#tnombreEquipo').val(), marca:marca, modelo:modelo, especificacion:tespecificacion.value,  fechaC:tfechadeC.value, codigoexterno:tcodigoexterno.value, proveedor:tproveedor.value, factura:tfactura.value, piezas:pieza },
            async: false,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                 console.log(msg);
                 if(msg=="true"){
                   /* limpiar();*/
                   $('#tnombreEquipo').val(0);
                   $('#tespecificacion').val(0); 
                  /* $('#testado').val(0); */ 
                   $('#tfechadeC').val(""); 
                  /* $('#tfechadeA').val("");*/
                  /* $('#tasignadopor').val(0);
                   $('#tnuevoresp').val(0);*/
                   /*$('#tArea').val(0);*/
                   tcodigoexterno.value="";
                   tcodigoInterno.value="";
                   $('#tproveedor').val(0); 
                   tfactura.value="";
                   $('#ma').val(0);
                   $('#mo').val(0);
                    $('.form-group').removeClass('has-success');
                    $('.form-group').removeClass('has-error');
                    $('.form-group').removeClass('has-error has-feedback');
                    $('.form-group').find('small.help-block').hide();
                    $('.form-group').find('i.form-control-feedback').hide();
                    alerta3.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Se ha registrado correctamente el equipo!</center></div>';
                    $('#cuerpotabla').html("");              
                    $('#sPiezas').empty();
                    /*$('option',  $('#sPiezas')).each(function(element) {
                        $(this).removeAttr('selected').prop('selected', false);
                    });
                     $('#sPiezas').multiselect('rebuild');*/

                    cargarlistadoPiezas();
                    /* $('#sPiezas').multiselect('refresh');*/

                   asignarId();
                   coverMarca.innerHTML="";
                   coverModelo.innerHTML="";


                 }
             
                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
        }
     });


    $('#guardar2').on('click',  function(){ 
      console.log(validar2());
        $.ajax
          ({
            type: "POST",
            url: "modelo/consultasEquiposyPiezas.php",
            data: {id:1, tipo:$('#mo2').val(), codigointerno:tcodigoInterno2.value,  marca:marca2, modelo:tnombreEquipo2.value,  fechaC:tfechadeC2.value, fechaA:tfechadeA2.value, asignado:tasignadopor2.value, responsable:tnuevoresp2.value, area:tArea2.value, codigoexterno:tcodigoE.value, proveedor:tproveedor2.value, factura:tfactura2.value,  codigoexterno:tcodigoE.value, idestado:$('#testado2').val(), idespecificacion:$('#tespecificacion2').val()},
            async: false,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                 console.log(msg);
                 if(msg=="true"){
                    limpiar();
                    alerta4.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Se ha registrado correctamente la pieza!</center></div>';
                    coverMarca2.innerHTML="";
                    coverModelo2.innerHTML="";
                   asignarId();

                 }
             
                   
                },
                error:
                function (msg) {console.log( msg +" No se pudo realizar la conexion");}
              });
    });
    
    $('#cancelar2').on('click',  function(){ 
      console.log("click en cancelar2"); 
      limpiar();
      alerta.innerHTML="";
      tnombreEquipo.value="";
                  $("#ma").val([]);
       $("#ma").attr('selectedIndex', '0').find("option:selected").removeAttr("selected");
       $("#ma").val(0);


    });

    function limpiar(){
     /* $('#tcantidad').val("");*/
     /* $('#tparte').val("");*/
      $('#tespecificacion2').val(0);
      $('#testado2').val(0);
      $('#tcodigoE').val("");

      $('#ma').val(0);
      $('#mo').val(0);
      $('#mo2').val(0);
      $('#ma2').val(0);
      $('#tnombreEquipo2').val(0);
      tcodigoInterno2.value="";



     tfechadeC2.value="";
     fechaA:tfechadeA2.value="";
     $('#tasignadopor2').val(0);
                   $('#tnuevoresp2').val(0);
                   $('#tArea2').val(0);
                   tcodigoE.value="";
                   $('#tproveedor2').val(0); 
                   tfactura2.value="";

     
      $('.form-group').removeClass('has-success');
      $('.form-group').removeClass('has-error');
      $('.form-group').removeClass('has-error has-feedback');
      $('.form-group').find('small.help-block').hide();
      $('.form-group').find('i.form-control-feedback').hide();
    }


    $('#nuevaMarca').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/marca.html', function(){

             $('#myModal').modal('show');

        });


    });

    $('#nuevaModelo').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/modelo.html', function(){

             $('#myModal').modal('show');

        });
        

    });

     $('#nuevaModeloD').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/tipo.html', function(){

             $('#myModal').modal('show');

        });
        

    });
    $('#nuevaEspecificacion').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/especificaciones.html', function(){

             $('#myModal').modal('show');

        });
        

    });
    $('#nuevaProveedor').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/proveedor.html', function(){

             $('#myModal').modal('show');

        });
        

    });


     $('#nuevaMarca2').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/marca.html', function(){

             $('#myModal').modal('show');

        });


    });
       $('#nuevaTipo').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/tipo.html', function(){

             $('#myModal').modal('show');

        });


    });

    $('#nuevaModelo2').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/modelo.html', function(){

             $('#myModal').modal('show');

        });
        

    });
    $('#nuevaEspecificacion2').on('click',  function(){  
        $("#personal2").load('partials/administrador/configuraciondelsistema/especificaciones.html', function(){

             $('#myModal2').modal('show');

        });
        

    });
    $('#nuevaProveedor2').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/proveedor.html', function(){

             $('#myModal').modal('show');

        });
        

    });

    $('#piezas2').on('click',  function(){  
       /* $('#historial2').removeClass('active');
        $('#historial').fadeOut();;*/

        console.log("click en piezas");
        $('#piezas2').attr('class', 'active');
        $('#piezas').fadeIn();


    });






          $('#cancelar').on('click',  function(){ 
                        console.log("click en cancelar"); 
                        $('#tnombreEquipo').val(0);

                        $('#ma').val(0);
                        $('#mo').val(0);
                        $('#tespecificacion').val(0);
                        $('#testado').val(0);
                        $('#tfechadeC').val("");
                        $('#tfechadeA').val("");
                        $('#tasignadopor').val(0);
                        $('#tnuevoresp').val(0);
                        $('#tArea').val(0);
                        $('#tproveedor').val(0);
                        tcodigoexterno.value="";
                        tfactura.value="";
                        $('.form-group').removeClass('has-success');
                        $('.form-group').removeClass('has-error');
                        $('.form-group').removeClass('has-error has-feedback');
                        $('.form-group').find('small.help-block').hide();
                        $('.form-group').find('i.form-control-feedback').hide();
                        coverMarca.innerHTML="";
                        coverModelo.innerHTML="";
                            /* console.log(marca);
                             console.log(modelo);*/

                        /*console.log(validar());*/
                         

               });







    function obtenerImagenMarca(marca, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:18, idmarca:marca },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                  
                    if(ban==1){
                      $('#coverMarca').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaMarca").css("display", "none");

                    } 
                    if(ban==2){
                      $('#coverMarca2').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca2.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaMarca2").css("display", "none");
                    }

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

    function obtenerImagenModelo(modelo, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:22, idmodelo:modelo },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {             
                  console.log("valor de ban: "+ban);
                    if(ban==1){
                      $('#coverModelo').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaModelo").css("display", "none");

                    } 
                    if(ban==2){
                      $('#coverModelo2').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo2.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaModelo2").css("display", "none");
                    }     

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

    function CargarMarcas(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:17},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                   

                    $('#ma').empty();
                    $('#ma2').empty();
                    $('#ma').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    $('#ma2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');

                    for (var i =0 ; i<msg[0].m; i++) {                       
                        $('#ma').append('<option  value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');
                        $('#ma2').append('<option  value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');

                        
                  }   
                      
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
  function cargarListaTipo(){
    $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:42},
           /* async: true,  */
            dataType: "json",  
            success:
            function (msg) 
            { 
                
                   $('#tnombreEquipo').empty();
                   $('#tnombreEquipo2').empty();
                    $('#tnombreEquipo').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                     $('#tnombreEquipo2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    


                for (var i =0 ; i<msg[0].m; i++) {        
                     
                      $('#tnombreEquipo').append('<option  value="'+msg[i].idE+'" >'+msg[i].motivo+'</option>');
                      $('#tnombreEquipo2').append('<option  value="'+msg[i].idE+'" >'+msg[i].motivo+'</option>');


                }
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
  }
    function cargarModelos(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:21},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                   $('#mo').empty();
                    $('#mo2').empty();
                    $('#mo').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    $('#mo2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {      
                        if(msg[i].idmodelo2==1||msg[i].idmodelo2==2||msg[i].idmodelo2==3)                 
                           $('#mo').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');
                         if(msg[i].idmodelo2!=1&&msg[i].idmodelo2!=2&&msg[i].idmodelo2!=3)
                            $('#mo2').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');


                    }                
                       /* $(".my-select").chosen({
                            width:"100%",
                          });*/
                         $( "#ma" ).change(function (e) {
                             e.preventDefault();
                             $("#cargaMarca").css("display", "block");
                             marca=$('#ma').val();
                             obtenerImagenMarca(marca, 1);
                             console.log("click en marca: "+marca);
                             ma=$("#ma :selected").text();
                             ma=ma[0]+ma[1];
                             tcodigoInterno.value=ma+mo+es+ID;


                         });
                         $( "#mo" ).change(function (e) {        
                            /* console.log($('#mo').val());*/
                            e.preventDefault();
                             $("#cargaModelo").css("display", "block");
                             modelo=$('#mo').val();
                              mo=$("#mo :selected").text();
                             mo=mo[0]+mo[1];
                             tcodigoInterno.value=ma+mo+es+ID;


                             obtenerImagenModelo(modelo, 1);
                         });  
                         $( "#ma2" ).change(function (e) {
                           /*  console.log($('#ma').val());*/
                             e.preventDefault();
                             $("#cargaMarca2").css("display", "block");
                             marca2=$('#ma2').val();
                              ma2=$("#ma2 :selected").text();
                             ma2=ma2[0]+ma2[1];
                             tcodigoInterno2.value=ma2+mo2+es2+ID2;
                             obtenerImagenMarca(marca2,2);

                         });
                         $( "#mo2" ).change(function (e) {        
                            /* console.log($('#mo').val());*/
                             e.preventDefault();
                             $("#cargaModelo2").css("display", "block");
                             modelo2=$('#mo2').val();
                              mo2=$("#mo2 :selected").text();
                             mo2=mo2[0]+mo2[1];
                             tcodigoInterno2.value=ma2+mo2+es2+ID2;
                             obtenerImagenModelo(modelo2,2);

                         });  
                         $( "#tespecificacion" ).change(function (e) {
                            
                             
                            es=$("#tespecificacion :selected").text();
                             es=es[0]+es[1];

                             tcodigoInterno.value=ma+mo+es+ID;
                           

                         });

                          $( "#tespecificacion2" ).change(function (e) {
                            
                             
                            es2=$("#tespecificacion2 :selected").text();
                             es2=es2[0]+es2[1];

                             tcodigoInterno2.value=ma2+mo2+es2+ID2;
                           

                         });

                    
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
    function cargarEspecificaciones(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:27},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    $('#tespecificacion').empty();
                    $('#tespecificacion2').empty();
                    $('#tespecificacion').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    $('#tespecificacion2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {  
                         $('#tespecificacion').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');
                          $('#tespecificacion2').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');
                    }              
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
        function cargarEstados(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:32},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    for (var i =0 ; i<msg[0].m; i++) {  
                         $('#testado').append('<option value="'+msg[i].idE+'" >'+msg[i].estado+'</option>');
                         $('#testado2').append('<option value="'+msg[i].idE+'" >'+msg[i].estado+'</option>');

                    }     

                    $('#testado').val(1);
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

    function cargarlistadoPersonal(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:2},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    for (var i =0 ; i<msg[0].m; i++) {  
                         $('#tasignadopor').append('<option value="'+msg[i].idpersonal+'" >'+msg[i].nombre+'   ' +msg[i].apellido+'</option>');
                          $('#tnuevoresp').append('<option value="'+msg[i].idpersonal+'" >'+msg[i].nombre+'   ' +msg[i].apellido+'</option>');
                          $('#tasignadopor2').append('<option value="'+msg[i].idpersonal+'" >'+msg[i].nombre+'   ' +msg[i].apellido+'</option>');
                          $('#tnuevoresp2').append('<option value="'+msg[i].idpersonal+'" >'+msg[i].nombre+'   ' +msg[i].apellido+'</option>');
                    }                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });

    }
    function cargarlistadoAreas(){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:12},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
              for (var i =0 ; i<msg[0].m; i++) {  
                   $('#tArea').append('<option value="'+msg[i].idareas+'" >'+msg[i].nombre+'</option>');
                   $('#tArea2').append('<option value="'+msg[i].idareas+'" >'+msg[i].nombre+'</option>');

              }
              $('#tArea').val(1);
      
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }
    function cargarlistadoProveedor(){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:7},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                    $('#tproveedor').empty();
                    $('#tproveedor2').empty();
                    $('#tproveedor').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    $('#tproveedor2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                for (var i =0 ; i<msg[0].m; i++) {  
                   $('#tproveedor').append('<option value="'+msg[i].idproveedor+'" >'+msg[i].nombre+'</option>');
                   $('#tproveedor2').append('<option value="'+msg[i].idproveedor+'" >'+msg[i].nombre+'</option>');

              }
      
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }


    function cargarlistadoPiezas(){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasEquiposyPiezas.php",
            data: {id:2},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
               console.log(msg);
              /*  $('#sPiezas').multiselect('deselectAll', true);
                 $('#example-rebuild').multiselect('rebuild');*/
                 $('#sPiezas').empty();
                for (var i =0 ; i<msg[0].m; i++) {  
                   $('#sPiezas').append('<option value="'+msg[i].idpieza+'" >'+msg[i].parte+'</option>');
              }
                 /*$('#sPiezas').multiselect({
                        enableFiltering: true,
                          filterPlaceholder: 'Buscar',

                      onChange: function(option, checked) {
                          var selectedOptions = $('#sPiezas option:selected');      
                          if (selectedOptions.length >=50) {                
                              var nonSelectedOptions = $('#sPiezas option').filter(function() {
                                  return !$(this).is(':selected');
                              });
               
                              var dropdown = $('#sPiezas').siblings('.multiselect-container');
                              nonSelectedOptions.each(function() {
                                  var input = $('input[value="' + $(this).val() + '"]');
                                  input.prop('disabled', true);
                                  input.parent('li').addClass('disabled');
                              });
                          }
                          else {
                              var dropdown = $('#sPiezas').siblings('.multiselect-container');
                              $('#sPiezas option').each(function() {
                                  var input = $('input[value="' + $(this).val() + '"]');
                                  input.prop('disabled', false);
                                  input.parent('li').addClass('disabled');
                              });
                          }
                      }
                       
                  });*/

      
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }
   function asignarId(){

   $.ajax
        ({
        type: "POST",
        url: "modelo/consultasEquiposyPiezas.php",
        data: {id:4},
        async: true,   
        dataType: "json",   
        success:
        function (msg) 
        {   
            ID=msg;
            asignarId2();

         },
            error:
            function (msg) {console.log( msg +"No se pudo realizar la conexion");}
        });

 }
    function asignarId2(){

   $.ajax
        ({
        type: "POST",
        url: "modelo/consultasEquiposyPiezas.php",
        data: {id:7},
        async: true,   
        dataType: "json",   
        success:
        function (msg) 
        {   

            ID2=msg;

         },
            error:
            function (msg) {console.log( msg +"No se pudo realizar la conexion");}
        });
 }


    function validar(){
      console.log(banpiezas);
        if(marca==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija una marca!</center></div>';        
           return false;
        }
        else if(modelo==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija una modelo!</center></div>';        

           return false;

        }/*
        else if(banpiezas==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija al menos una pieza!</center></div>';        

           return false;

        }*/
        else {return true;}
    }
    function validar2(){
      console.log(banpiezas);
        if(marca2==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija una marca!</center></div>';        
           return false;
        }
        else if(modelo2==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija una modelo!</center></div>';        

           return false;

        }
        /*else if(banpiezas2==-1){
            alerta3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Por favor elija al menos una pieza!</center></div>';        

           return false;

        }*/
        else {return true;}
    }

 });


</script>
