 <section id="Pieza" style="display:none;">
    <div class="panel panel-default">
      <div class="panel-heading"> <b><h4>Agregar Pieza</h4></b></div>
      <div class="panel-body">
        
           <br><br>
            <form id="formregistrar3"  method="post" class="form-horizontal"  onsubmit="return false;"> 
                <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                  <div class="form-group">
                         <label class="col-sm-4 control-label">Tipo:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  id="mo2" class="my-select form-control input-sm" name="modelo2">
                                    <option value="0"  disabled selected> --Seleccione una opcion -- </option>                                  
                                </select>            
                                
                        </div>
                         <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaModelo3" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarModelo2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>

                      </div>
                    
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Marca:</label>
                          <div class="col-sm-6" style="padding-right:0px;">                             
                               <select  id="ma2" class="my-select form-control input-sm" name="marca2">
                                    <option value="0" > --Seleccione una opcion -- </option>
                                </select> 
                                
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaMarca2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarMarca2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Modelo </label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  id="tnombreEquipo2" class="form-control input-sm" name="nombreEquipo2">
                                    <option value="0" > --Seleccione una opcion -- </option>                                

                              </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaTipo" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarTipo" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Especificacion:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="especificacion2"  id="tespecificacion2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaEspecificacion2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarEspecificacion2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Estado:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="estado2"  id="testado2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                           <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaEstado" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarEstado" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Fecha de Compra:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tfechadeC2" type="date" class="form-control input-sm" name="fechadeC2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>

                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Fecha de Asignacion :</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <input  id="tfechadeA2" type="date" class="form-control input-sm" name="fechadeA2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                     <!--  -->
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Asignado por:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="asignadopor2"  id="tasignadopor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div> 
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Nuevo Responsable:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="nuevoresp2"  id="tnuevoresp2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Area:</label>
                         <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="area2"  id="tArea2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Externo:</label>
                          <div class="col-sm-6" style="padding-right:0px;">  
                              <input  id="tcodigoE" type="text" class="form-control input-sm" name="codigoE">                        
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Proveedor:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="proveedor2"  id="tproveedor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" class="btn btn-default btn-sm" id="nuevaProveedor2" title="Crear Nuevo Modelo"><span class="glyphicon glyphicon-plus" ></span></button>
                         </div>
                           <div class="col-sm-1" style="padding-left:0px;">                                
                                <button type="button" class="btn btn-default btn-sm" id="actualizarProveedor2" title="Actualizar Combo"><span class="glyphicon glyphicon-refresh" ></span></button>

                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Nª de Factura:</label>
                          <div class="col-sm-6" style="padding-right:0px;"> 
                             <input  id="tfactura2" type="text" class="form-control input-sm" name="emfactura2">  
                             
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      
                </div>
                <div class="col-md-1 col-xs-1 col-sm-1"> 
                </div>

                <div class="col-md-5 col-xs-5 col-sm-5"> 
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Interno:</label>
                          <div class="col-sm-6">
                              <input  id="tcodigoInterno2" type="text" class="form-control input-sm" name="codigoInterno2" disabled="true">  
                              
                          </div>
                      </div>
                         <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Marca</small></div>                     
                            <div class="panel-body">
                                 <div id="cargaMarca2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverMarca2"></div>
                                  
                                </center>
                             </div>
                      </div>
                      <br>
                       <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Tipo</small></div>                     
                            <div class="panel-body">
                                <div id="cargaModelo2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverModelo2"></div>
                                  
                                </center>
                             </div>
                      </div>
                     
                </div>
            </div>

            <hr>
                  <center>
                     <div class="row">                        
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                     <button type="submit" class="btn btn-primary" id="guardar2"  disabled="disabled"><span class="glyphicon glyphicon-floppy-disk" ></span>Registrar</button>                      
                                   
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                              <section id="alerta4"></section>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                    <button type="button" class="btn btn-danger" id="cancelar2"><span class="glyphicon glyphicon-trash"></span>Cancelar</button>  
                                                          
                            </div>                      
                      </div>          
                </center>                  
            </form>
      </div>
    </div>
  </section>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Nuevo</h4>
      </div>
      <div class="modal-body" id="personal2">
     
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>


 <script type="text/javascript">
  $(document).ready(function() {
  $('#menuprincipal').on('click',  function(){  window.open('index.php' , '_self');});
    var marca="-1", modelo="-1",  marca2="-1", modelo2="-1", banpiezas="-1";
    var ma="", ma2="", mo="", mo3="", es="", es2="", ID="", ID2=""; 




$('#formregistrar3').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                  nombreEquipo2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                  marca2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                modelo2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                 }
                },
                especificacion2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                especificacion2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeC2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                fechadeA2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
             
                
                proveedor2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                },
                emfactura2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             digits: {
                                   message: 'Por favor introduce sólo dígitos'
                              },
                              stringLength: {
                                   max: 9,
                                  message: 'Por favor introduce menos de %s caracteres'
                              }  
                      }
                },                 
               
                codigoE:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }
                      }
                }
            }
              
          })
            .on('success.field.fv', function(e, data) {   
               if (!data.fv.isValid()) {    // There is invalid field
                     data.fv.disableSubmitButtons(true);     
                    
                  }
            });

            CargarMarcas();    
    cargarEspecificaciones();
    cargarEstados();
    cargarlistadoPersonal();
    cargarlistadoAreas();
    cargarlistadoProveedor();
    cargarlistadoPiezas();
    asignarId();
    cargarListaTipo();
    cargarModelos();
     

var table = $('<table style="width:100%;" class="table" ></table>');                         
var row=$('<thead></thead>').append("<tr></tr>");
row.append($('<th></th>').html(""));
/*row.append($('<th></th>').html("<b>Cantidad</b>")); */                           
row.append($('<th></th>').html("<b>Tipo</b>"));
row.append($('<th></th>').html("<b>Estado</b>"));
row.append($('<th></th>').html("<b>Codigo Externo</b>"));
row.append($('<th></th>').html("<b>Especificacion</b>"));
row.append($('<th></th>').html("<b>Marca</b>"));
row.append($('<th></th>').html("<b>Modelo</b>"));

var row2 = $('<tbody id="cuerpotabla"></tbody>');
table.append(row2);


$('#listadopiezas').append(table);

table.append(row);

});



      $('#sPiezas').change(function () {
          if(!$('#sPiezas').val()==false){
             banpiezas=$('#sPiezas').val();
              $.ajax
              ({
                type: "POST",
                url: "modelo/consultasEquiposyPiezas.php",
                data: {id:3, ids:$('#sPiezas').val()},
                async: false,  
                dataType: "json",  
                success:
                function (msg) 
                { 
                    
                    $('#cuerpotabla').html("");              
                    var row3="";
                    console.log("tamano: "+msg[0].m);
                    if(msg[0].m==0){
                      alert("Escoja otra pieza, lo que selecciono ya no esta disponible");
                    }
                    for (var i = 0; i <msg[0].m; i++) {
                        console.log(i);
                        var row3=$('<tr id="'+msg[i].idpieza+'"></tr>');
                     
                        var fila0=$('<td></td>').text(i);
                      /*  var fila1 = $('<td></td>').text(msg[i].cantidad);*/
                        var fila2 = $('<td></td>').text(msg[i].parte);
                        var fila3 = $('<td></td>').text(msg[i].estado);
                        var fila4 = $('<td></td>').text(msg[i].codigoexterno);
                        var fila5 = $('<td></td>').text(msg[i].especificacion);
                        var fila6 = $('<td></td>').text(msg[i].nombremarca);
                        var fila7 = $('<td></td>').text(msg[i].nombremodelo);

                         row3.append(fila0);
                       /* row3.append(fila1);*/
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);
                        row3.append(fila5);
                        row3.append(fila6);
                        row3.append(fila7);

                      $('#cuerpotabla').append(row3); 
                }            
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
          }
          else{
                $('#cuerpotabla').html("");
                banpiezas="-1";
          }






       $('#actualizarMarca2').on('click',  function(){
          CargarMarcas();
       });
        $('#actualizarModelo2').on('click',  function(){
          cargarModelos();
       });

      $('#actualizarEspecificacion2').on('click',  function(){
        cargarEspecificaciones();
       });

      $('#actualizarProveedor2').on('click',  function(){
        cargarlistadoProveedor();
       });

      $('#actualizarTipo').on('click',  function(){
        cargarListaTipo();
       });


       $('#nuevaMarca2').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/marca.html', function(){

             $('#myModal').modal('show');

        });


    });
       $('#nuevaTipo').on('click',  function(){  
        $("#personal").load('partials/administrador/configuraciondelsistema/modelo.html', function(){

             $('#myModal').modal('show');

        });


    });

    $('#nuevaModelo3').on('click',  function(){  
        $("#personal2").load('partials/administrador/configuraciondelsistema/modelo.html', function(){

             $('#myModal2').modal('show');

        });
        

    });
    $('#nuevaEspecificacion2').on('click',  function(){  
        $("#personal2").load('partials/administrador/configuraciondelsistema/especificaciones.html', function(){

             $('#myModal2').modal('show');

        });
        

    });
    $('#nuevaProveedor2').on('click',  function(){  
        $("#personal2").load('partials/administrador/configuraciondelsistema/proveedor.html', function(){

             $('#myModal2').modal('show');

        });
        

    });
    


         function cargarModelos(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:21},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                  /* $('#mo').empty();*/
                    $('#mo3').empty();
                  /*  $('#mo').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');*/
                    $('#mo3').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {      
                /*        if(msg[i].idmodelo2==1||msg[i].idmodelo2==2||msg[i].idmodelo2==3)                 
                           $('#mo').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');*/
                         if(msg[i].idmodelo2!=1&&msg[i].idmodelo2!=2&&msg[i].idmodelo2!=3)
                            $('#mo3').append('<option  value="'+msg[i].idmodelo2+'" >'+msg[i].nombre+'</option>');


                    }                
                       
                         $( "#ma2" ).change(function (e) {
                             e.preventDefault();
                             $("#cargaMarca2").css("display", "block");
                             marca2=$('#ma2').val();
                              ma2=$("#ma2 :selected").text();
                             ma2=ma2[0]+ma2[1];
                             tcodigoInterno2.value=ma2+mo3+es2+ID2;
                             obtenerImagenMarca(marca2,2);

                         });
                         $( "#mo3" ).change(function (e) {        
                             e.preventDefault();
                             $("#cargaModelo2").css("display", "block");
                             modelo2=$('#mo3').val();
                              mo3=$("#mo3 :selected").text();
                             mo3=mo3[0]+mo3[1];
                             tcodigoInterno2.value=ma2+mo3+es2+ID2;
                             obtenerImagenModelo(modelo2,2);

                         });  
                 
                          $( "#tespecificacion2" ).change(function (e) {
                            
                             
                            es2=$("#tespecificacion2 :selected").text();
                             es2=es2[0]+es2[1];

                             tcodigoInterno2.value=ma2+mo3+es2+ID2;
                           

                         });

                    
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

        function obtenerImagenMarca(marca, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:18, idmarca:marca },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                  
                   /* if(ban==1){
                      $('#coverMarca').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaMarca").css("display", "none");

                    } */
                    if(ban==2){
                      $('#coverMarca2').innerHTML="";
                       var path="modelo/uploads/marca/"+msg[0].idmarca+"/"+msg[0].imagen;
                       coverMarca2.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaMarca2").css("display", "none");
                    }

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
        function obtenerImagenModelo(modelo, ban){
               $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:22, idmodelo:modelo },
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {             
                  console.log("valor de ban: "+ban);
                   /* if(ban==1){
                      $('#coverModelo').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaModelo").css("display", "none");

                    } */
                    if(ban==2){
                      $('#coverModelo2').innerHTML="";
                       var path="modelo/uploads/modelo/"+msg[0].idmodelo+"/"+msg[0].imagen;
                       coverModelo2.innerHTML='<img src="'+path+ '"  width="150" height="150" />'; 
                       $("#cargaModelo2").css("display", "none");
                    }     

                    
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
    function CargarMarcas(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:17},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                {                   

                    /*$('#ma').empty();
*/                    $('#ma2').empty();
                 /*   $('#ma').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');*/
                    $('#ma2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');

                    for (var i =0 ; i<msg[0].m; i++) {                       
                       /* $('#ma').append('<option  value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');*/
                        $('#ma2').append('<option  value="'+msg[i].idmarca2+'" >'+msg[i].nombre+'</option>');

                        
                  }   
                      
                         
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }
  function cargarListaTipo(){
    $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:42},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                
                for (var i =0 ; i<msg[0].m; i++) {        
                     
                     /* $('#tnombreEquipo').append('<option  value="'+msg[i].idE+'" >'+msg[i].motivo+'</option>');*/
                      $('#tnombreEquipo2').append('<option  value="'+msg[i].idE+'" >'+msg[i].motivo+'</option>');


                }
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
  }

   function cargarEspecificaciones(){
        $.ajax
            ({
              type: "POST",
              url: "modelo/consultasconfiguraciondelsistema.php",
              data: {id:27},
              async: true,  
              dataType: "json",  
              success:
              function (msg) 
                { 
                    /*$('#tespecificacion').empty();*/
                    $('#tespecificacion2').empty();
                  /*  $('#tespecificacion').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');*/
                    $('#tespecificacion2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {  
                        /* $('#tespecificacion').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');*/
                          $('#tespecificacion2').append('<option value="'+msg[i].idE+'" >'+msg[i].especificacion+'</option>');
                    }              
                },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });
    }

    function cargarlistadoProveedor(){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:7},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                   /* $('#tproveedor').empty();*/
                    $('#tproveedor2').empty();
                  /*  $('#tproveedor').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');*/
                    $('#tproveedor2').append('<option value="0" disabled selected> --Seleccione una opcion -- </option>');
                for (var i =0 ; i<msg[0].m; i++) {  
                  /* $('#tproveedor').append('<option value="'+msg[i].idproveedor+'" >'+msg[i].nombre+'</option>');*/
                   $('#tproveedor2').append('<option value="'+msg[i].idproveedor+'" >'+msg[i].nombre+'</option>');

              }
      
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }

  function asignarId2(){

   $.ajax
        ({
        type: "POST",
        url: "modelo/consultasEquiposyPiezas.php",
        data: {id:7},
        async: true,   
        dataType: "json",   
        success:
        function (msg) 
        {   

            ID2=msg;

         },
            error:
            function (msg) {console.log( msg +"No se pudo realizar la conexion");}
        });
 }

});
