 
  <section >
            <div id="alertas"> 
                <div class="alert alert-info">
                        <center>
                                <h6><b>Modificar Equipo o Pieza</b></h6>                      
                        </center>
                </div>
            </div>
        </section>

   <form id="formr"  method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">  
                     <div class="form-group">
                         <label class="col-sm-4 control-label">Modificar</label>
                          <div class="col-sm-8">
                              <select  class="form-control input-sm"  name="tipo"  id="ttipo"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                    <option value="1">Equipo</option>
                                    <option value="2">Pieza</option>
                               </select> 
                          </div>
                      </div>
                </div>
            </div>
    </form>

    <form id="formregistrar"  method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
    

    <section id="panelBuscar" style="display:none;">                
                 <div class="form-group">
                  <label class="col-sm-2 control-label">Por Area:</label>
                    <div class="col-md-3 col-xs-3 col-sm-3">
                   
                        <select  class="form-control input-sm"  name="area2"  id="tArea2"  >
                              <option value="0"disabled selected> --Seleccione una opcion -- </option>
                             
                         </select> 
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1">
                        <button type="submit" class="btn btn-primary" id="Bbuscar" ><span class="glyphicon glyphicon-search"></span></button>   

                    </div>
                  <label class="col-sm-2 control-label">Por codigo interno:</label>
                  <div class="col-md-3 col-xs-3 col-sm-3">
                   
                        <input  id="tbuscar2" type="text" class="form-control input-sm" name="usuario2" value="" placeholder="Escriba el codigo"/> 
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1">
                        <button type="submit" class="btn btn-primary" id="Bbuscar2" ><span class="glyphicon glyphicon-search"></span></button>   

                    </div>
                  </div>
                  
                  <div id="labelbusqueda" >
                    
                  </div>
     </section>

     <section id="panelBuscarPieza" style="display:none;">
            
            <div class="form-group">
                  <label class="col-sm-2 control-label">Por Area:</label>
                    <div class="col-md-3 col-xs-3 col-sm-3">
                   
                        <select  class="form-control input-sm"  name="area3"  id="tArea3"  >
                              <option value="0"disabled selected> --Seleccione una opcion -- </option>
                             
                         </select> 
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1">
                        <button type="submit" class="btn btn-primary" id="Bbuscar3" ><span class="glyphicon glyphicon-search"></span></button>   

                    </div>
                  <label class="col-sm-2 control-label">Por codigo interno:</label>
                  <div class="col-md-3 col-xs-3 col-sm-3">
                   
                        <input  id="tbuscar3" type="text" class="form-control input-sm" name="usuario3" value="" placeholder="Escriba el codigo"/> 
                    </div>
                    <div class="col-md-1 col-xs-1 col-sm-1">
                        <button type="submit" class="btn btn-primary" id="Bbuscar4" ><span class="glyphicon glyphicon-search"></span></button>   

                    </div>
                  </div>
                  
                  <div id="labelbusqueda2" >
                    
                  </div>
     </section>
     <section id="listadoBusqueda"></section>
     </form>









<div class="modal fade" id="ModalPieza" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modificar Pieza</h4>
      </div>
      <div class="modal-body " id="cuerpoPieza">
      

         <form id="formregistrar3"  method="post" class="form-horizontal"  onsubmit="return false;"> 
                <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                  <div class="form-group">
                         <label class="col-sm-4 control-label">Tipo:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  id="mo2" class="my-select form-control input-sm" name="modelo2">
                                    <option value="0"  disabled selected> --Seleccione una opcion -- </option>                                  
                                </select>            
                                
                        </div>
                        

                      </div>
                    
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Marca:</label>
                          <div class="col-sm-8" style="padding-right:0px;">                             
                               <select  id="ma2" class="my-select form-control input-sm" name="marca2">
                                    <option value="0" > --Seleccione una opcion -- </option>
                                </select> 
                                
                          </div>
                          
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Modelo </label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  id="tnombreEquipo2" class="form-control input-sm" name="nombreEquipo2">
                                    <option value="0" > --Seleccione una opcion -- </option>                                

                              </select> 
                          </div>
                       
                      </div>
                       
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Especificacion:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="especificacion2"  id="tespecificacion2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                         
                      </div>
                       <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Estado:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="estado2"  id="testado2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                         
                      </div>
                       <div class="form-group">
                         <label class="col-sm-4 control-label">Fecha de Compra:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <input  id="tfechadeC2" type="date" class="form-control input-sm" name="fechadeC2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>

                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Fecha de Asignacion :</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <input  id="tfechadeA2" type="date" class="form-control input-sm" name="fechadeA2"  max=<?php  echo date("Y-m-d"); ?>>  
                              
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                     <!--  -->
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Asignado por:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="asignadopor2"  id="tasignadopor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div> 
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Nuevo Responsable:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="nuevoresp2"  id="tnuevoresp2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group" style="display:none;">
                         <label class="col-sm-4 control-label">Area:</label>
                         <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="area2"  id="tArea2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Externo:</label>
                          <div class="col-sm-8" style="padding-right:0px;">  
                              <input  id="tcodigoE" type="text" class="form-control input-sm" name="codigoE">                        
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Proveedor:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                              <select  class="form-control input-sm"  name="proveedor2"  id="tproveedor2"  >
                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                   
                               </select> 
                          </div>

                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Nª de Factura:</label>
                          <div class="col-sm-8" style="padding-right:0px;"> 
                             <input  id="tfactura2" type="text" class="form-control input-sm" name="emfactura2">  
                             
                          </div>
                          <div class="col-sm-1" style="padding-left:0px;">
                          </div>
                      </div>
                      
                </div>
                <div class="col-md-1 col-xs-1 col-sm-1"> 
                </div>

                <div class="col-md-5 col-xs-5 col-sm-5"> 
                    <div class="form-group">
                         <label class="col-sm-4 control-label">Codigo Interno:</label>
                          <div class="col-sm-8">
                              <input  id="tcodigoInterno2" type="text" class="form-control input-sm" name="codigoInterno2" disabled="true">  
                              
                          </div>
                      </div>
                         <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Marca</small></div>                     
                            <div class="panel-body">
                                 <div id="cargaMarca2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverMarca2"></div>
                                  
                                </center>
                             </div>
                      </div>
                      <br>
                       <div class="panel panel-default">   
                          <div class="panel-heading"> <small>Cover Tipo</small></div>                     
                            <div class="panel-body">
                                <div id="cargaModelo2" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                <center>

                                  <div id="coverModelo2"></div>
                                  
                                </center>
                             </div>
                      </div>
                     
                </div>
            </div>

            <hr>
                  <center>
                    <section id="alerta4"></section>
                       <div class="row" id="seccioncontrol2">                        
                                                   
                        </div>          
                  </center>

            </form>




      </div>      
    </div>
  </div>
</div>





















<div class="modal fade" id="ModalEquipo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modificar/Eliminar Equipo</h4>
      </div>
      <div class="modal-body " id="cuerpoEquipo">
             <section id="Equipo" style="display:block;">
                   
                 
                  
                    <form id="formregistrar2"  method="post" class="form-signin form-horizontal"  onsubmit="return false;">  

                             <div class="row">
                                <div class="col-md-6 col-xs-6 col-sm-6">
                                   <div class="form-group" >
                                         <label class="col-sm-4 control-label">Tipo:</label>
                                          <div class="col-sm-8">
                                              <select  id="mo" class="form-control input-sm" name="modelo" disabled="true">
                                                    <option value="0"  disabled selected> --Seleccione una opcion -- </option>                                  
                                                </select> 
                                         
                                        </div>

                                      </div>
                                    <div class="form-group">
                                         <label class="col-sm-4 control-label">Marca:</label>
                                          <div class="col-sm-8">                             
                                               <select  id="ma" class="form-control input-sm" name="marca" disabled="true">
                                                    <option value="0" > --Seleccione una opcion -- </option>
                                                </select> 
                                               
                                              </div>
                                      </div>
                                       <div class="form-group">
                                         <label class="col-sm-4 control-label">Modelo:</label>
                                          <div class="col-sm-8">
                                              <select  id="tnombreEquipo" class="form-control input-sm" name="nombreEquipo" disabled="true">
                                                    <option value="0" disabled selected> --Seleccione una opcion -- </option>
                                                    

                                              </select> 
                                          </div>
                                      </div>
                                       
                                       <div class="form-group">
                                         <label class="col-sm-4 control-label">Especificacion:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="especificacion"  id="tespecificacion"  disabled="true">
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                       <div class="form-group" id="labeestado" style="display:none;">
                                         <label class="col-sm-4 control-label">Estado:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="estado"  id="testado"  >
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                       <div class="form-group">
                                         <label class="col-sm-4 control-label">Fecha de Compra:</label>
                                          <div class="col-sm-8">
                                              <input  id="tfechadeC" type="date" class="form-control input-sm input-sm" name="fechadeC" disabled="true"  max=<?php  echo date("Y-m-d"); ?> />
                                              
                                          </div>
                                      </div>
                                      <div class="form-group" id="labelfechaasignacion" style="display:none;">
                                         <label class="col-sm-4 control-label">Fecha de asignacion:</label>
                                          <div class="col-sm-8">
                                              <input  id="tfechadeA" type="date" class="form-control input-sm" name="fechadeA" max=<?php  echo date("Y-m-d"); ?>  />
                                              
                                          </div>
                                      </div>
                                       <div class="form-group" id="labelasignadopor" style="display:none;">
                                         <label class="col-sm-4 control-label"> Asignado por:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="asignadopor"  id="tasignadopor"  >
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div> 
                                      <div class="form-group" id="labelnuevoresp" style="display:none;">
                                         <label class="col-sm-4 control-label" >Nuevo Responsable:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="nuevoresp"  id="tnuevoresp"  >
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                      <div class="form-group" id="labelarea" style="display:none;">
                                         <label class="col-sm-4 control-label" >Area:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="area"  id="tArea"  >
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                      <div class="form-group" id="labelMotivo" style="display:none;">
                                         <label class="col-sm-4 control-label" >Motivo:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="Motivo"  id="tMotivo"  >
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                      <div class="form-group">
                                         <label class="col-sm-4 control-label">Codigo Externo:</label>
                                          <div class="col-sm-8">
                                             <input  id="tcodigoexterno" type="text" class="form-control input-sm" name="codigoexterno" disabled="true">
                                              
                                          </div>
                                      </div>
                                      <div class="form-group">
                                         <label class="col-sm-4 control-label">Proveedor:</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="proveedor"  id="tproveedor"  disabled="true">
                                                    <option value="0"disabled selected> --Seleccione una opcion -- </option>
                                                   
                                               </select> 
                                          </div>
                                      </div>
                                      <div class="form-group">
                                         <label class="col-sm-4 control-label">Nª de Factura:</label>
                                          <div class="col-sm-8">
                                             <input  id="tfactura" type="text" class="form-control input-sm" name="emfactura" >
                                             
                                          </div>
                                      </div>
                                      <div class="form-group" id="labelPersonal" style="display:none;">
                                         <label class="col-sm-4 control-label">Personal</label>
                                          <div class="col-sm-8">
                                             <!-- <input  id="tpersonalnuevo" type="text" class="form-control input-sm" name="empersonalnuevo"> -->  
                                             <select  id="tpersonalnuevo" type="text" class="form-control input-sm"  name="personalnuevo" >
                                                    <option value="0" disabled selected> --Seleccione una opcion -- </option>          
                                             </select> 
                                          </div>
                                      </div>
                                      <div class="form-group" id="labelfecharealizado" style="display:none;">
                                         <label class="col-sm-4 control-label">Fecha de realizacion:</label>
                                          <div class="col-sm-8">
                                              <input  id="tfechadeR" type="date" class="form-control input-sm " name="fechadeR" max=<?php  echo date("Y-m-d"); ?>> 
                                              
                                          </div>
                                      </div>
                                      <div class="form-group">
                                         <label class="col-sm-4 control-label">Accion</label>
                                          <div class="col-sm-8">
                                              <select  class="form-control input-sm"  name="accion"  id="taccion"  >
                                                    <option value="0" disabled selected> --Seleccione una opcion -- </option>                       
                                               </select>                                           
                                          </div>
                                      </div>
                                  </div>


                                <div class="col-md-1 col-xs-1 col-sm-1"> 
                                </div>

                                <div class="col-md-5 col-xs-5 col-sm-5"> 
                                    <div class="form-group">
                                         <label class="col-sm-4 control-label">Codigo Interno:</label>
                                          <div class="col-sm-8">
                                              <input  id="tcodigoInterno" type="text" class="form-control input-sm" name="codigoInterno" disabled="true">  
                                              
                                          </div>
                                      </div>
                                       <div class="panel panel-default">   
                                          <div class="panel-heading"> <small>Cover Marca</small></div>                     
                                            <div class="panel-body">
                                                 <div id="cargaMarca" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                                <center>

                                                  <div id="coverMarca"></div>
                                                  
                                                </center>
                                             </div>
                                      </div>
                                      <br>
                                       <div class="panel panel-default">   
                                          <div class="panel-heading"> <small>Cover Tipo</small></div>                     
                                            <div class="panel-body">
                                                <div id="cargaModelo" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
                                                <center>

                                                  <div id="coverModelo"></div>
                                                  
                                                </center>
                                             </div>
                                      </div>

                                      
                                </div>
                            </div><hr>

                            <section id="piezas3">              
                              <div role="tabpanel">        
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active" id="piezas2"><a href="#piezas" aria-controls="piezas" role="tab" data-toggle="tab">Piezas</a></li>
                                  <li role="presentation" id="historial2"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Acciones</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="piezas">
                                    
                                    <div class="panel panel-default">
                                        
                                      <div class="panel-body">
                                             <div id="taphistorial" style="display:none;"></div> 
                                          <div id="tappiezas">

                                                    <div class="form-group" id="labelspiezas" style="display:none;">
                                                       <label class="col-sm-3 control-label">Seleccione un Componente:</label>
                                                        <div class="col-sm-8">
                                                             <select  class="form-control"  name="priority" multiple="multiple" id="sPiezas"></select>                                           
                                                        </div>
                                                        <div class="col-sm-1" style="padding-left:0px;">                                
                                                                <button type="button" class="btn btn-default btn-xs" id="actualizarsPiezas" title="Actualizar Componentes Disponibles"><span class="glyphicon glyphicon-refresh" ></span></button>

                                                          </div>
                                                    </div>
                                                 <div class="form-group" id="labelspiezas2" style="display:none;">
                                                       <label class="col-sm-3 control-label">Componentes del Equipo:</label>
                                                        <div class="col-sm-8">
                                                             <select  class="form-control"  name="priority" multiple="multiple" id="sPiezas2"></select>                                           
                                                        </div>
                                                         <div class="col-sm-1" style="padding-left:0px;">
                                                                <button type="button" class="btn btn-default btn-xs" id="comp2" title="Quitar Pieza"><span class="glyphicon glyphicon-minus" ></span></button>
                                                         </div>
                                                          <div class="col-sm-1" style="padding-left:0px;">                                
                                                                <button type="button" class="btn btn-default btn-xs" id="actualizarcomp2" title="Componentes del Equipo"><span class="glyphicon glyphicon-refresh" ></span></button>

                                                          </div>
                                                    </div>

                                           <div class="row">                                
                                                <div class="col-md-12 col-xs-12 col-sm-12">
                                                      <div class="panel panel-default">
                                                        <div class="panel-heading"> <small>Seleccionados</small></div>
                                                         <div class="panel-body" id="listadopiezas">

                                                           </div>
                                                      </div>
                                                </div>
                                            </div>
                                            </div>

                                      </div>
                                    </div>

                                    <div id="historial">
                                    
                                    </div>

                                  </div>
                                  
                                </div>
                            </section>

                                 <center>
                                  <section id="alerta3"></section>
                                     <div class="row" id="seccioncontrol">                        
                                                                 
                                      </div>          
                                </center>
                            </form>

                          
                      </section>
      </div>
      
    </div>
  </div>
</div>




<script type="text/javascript" src="js/eventosbuscarpieza.js"></script>
<script type="text/javascript" src="js/eventosbuscarequipo.js"></script>
 <script type="text/javascript">
    $(document).ready(function() {
/*   $('.date-pick').datePicker(
    {
      startDate: '01/01/1970',
      endDate: (new Date()).asString()
    }
    );*/
cargarlistadoAreas();

    $('#ttipo').change(function () {
     /* console.log("click: "+$('#ttipo').val());*/
        if($('#ttipo').val()==1){
          $('#panelBuscar').fadeIn();
          $('#panelBuscarPieza').fadeOut();
         listadoBusqueda.innerHTML="";

        }
        else{
          $('#panelBuscar').fadeOut();
          $('#panelBuscarPieza').fadeIn();
          listadoBusqueda.innerHTML="";

        }

    });

   


      function cargarlistadoAreas(){
      $.ajax
          ({
            type: "POST",
            url: "modelo/consultasconfiguraciondelsistema.php",
            data: {id:12},
            async: true,  
            dataType: "json",  
            success:
            function (msg) 
            {     console.log("cantidad de areas encontrados: "+msg[0].m);

                    $('#tArea2').empty();           
                   $('#tArea2').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                   $('#tArea3').empty();           
                   $('#tArea3').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                    for (var i =0 ; i<msg[0].m; i++) {  

                         $('#tArea2').append('<option value="'+msg[i].idareas+'" >'+msg[i].nombre+'</option>');
                         $('#tArea3').append('<option value="'+msg[i].idareas+'" >'+msg[i].nombre+'</option>');


                    }
             

      
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
    }

     $('#Bbuscar4').on('click',  function(){
         if(tbuscar3.value!=""){
            console.log("entro en buscar por codigo pieza");
              $.ajax
                    ({
                      type: "POST",
                      url: "modelo/consultasEquiposyPiezas.php",
                      data: {id:21, data:tbuscar3.value},
                      async: false,  
                      dataType: "json",  
                      success:
                      function (msg) 
                      { 
                          console.log(msg[0].m);
                            if(msg[0].m>=1){
                                 $('#listadoBusqueda').html("");                        

                                 labelbusqueda.innerHTML="";

                                $('#ModalPieza').modal('show'); 
                                eventoCargarPieza(msg);
                            }
                            if(msg[0].m==0){
                                labelbusqueda.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Pieza no encontrada!</center></div>';
                             }
                                              
                             
                      },
                      error:
                      function (msg) {console.log( msg +" No se pudo realizar la conexion");}
                    });
            
        }

     });
     $('#Bbuscar2').on('click',  function(){
         if(tbuscar2.value!=""){

              $.ajax
                    ({
                      type: "POST",
                      url: "modelo/consultasEquiposyPiezas.php",
                      data: {id:6, data:tbuscar2.value},
                      async: false,  
                      dataType: "json",  
                      success:
                      function (msg) 
                      { 
                          console.log(msg[0].m);
                            if(msg[0].m>=1){
                                 $('#listadoBusqueda').html("");                        

                                 labelbusqueda.innerHTML="";
                                $('#ModalEquipo').modal('show'); 
                                 eventoscargar(msg); 
                            }
                            if(msg[0].m==0){
                                labelbusqueda.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Equipo no encontrado!</center></div>';
                             }
                                              
                             
                      },
                      error:
                      function (msg) {console.log( msg +" No se pudo realizar la conexion");}
                    });
            
        }

     });
  function editar(){

       $('.editar').on('click',  function(){
          var id=$(this).attr('name');
          console.log("Valor a modificar: "+id);
           $.ajax
            ({
              type: "POST",
              url: "modelo/consultasEquiposyPiezas.php",
              data: {id:14, data:id},
              async: false,  
              dataType: "json",  
              success:
              function (msg) 
              { 
                  console.log("Encontrado: "+msg[0].m);
                  if(msg[0].m>=1){
                       labelbusqueda.innerHTML="";
                      $('#ModalEquipo').modal('show'); 
                       eventoscargar(msg); 
                  }
               },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });

       });
  }

 function editarPieza(){

       $('.editarPieza').on('click',  function(){
          var id=$(this).attr('name');
          console.log("Valor a modificar: "+id);
           $.ajax
            ({
              type: "POST",
              url: "modelo/consultasEquiposyPiezas.php",
              data: {id:19, data:id},
              async: false,  
              dataType: "json",  
              success:
              function (msg) 
              { 
                  console.log("Encontrado: "+msg);
                  if(msg[0].m>=1){
                       labelbusqueda.innerHTML="";
                      $('#ModalPieza').modal('show'); 
                       eventoCargarPieza(msg);
                  }
               },
              error:
              function (msg) {console.log( msg +" No se pudo realizar la conexion");}
            });

       });
  }
        $('#Bbuscar3').on('click',  function(){

            /*console.log(!$('#tArea2').val());*/
            if(!$('#tArea3').val()==false){
                  $.ajax
                    ({
                      type: "POST",
                      url: "modelo/consultasEquiposyPiezas.php",
                      data: {id:18, data:$('#tArea3').val()},
                      async: false,  
                      dataType: "json",  
                      success:
                      function (msg) 
                      { 
                          console.log(msg)
                         if(msg[0].m>0){
                                 labelbusqueda.innerHTML=''; 
                                 $('#listadoBusqueda').html("");                        
                                  var table = $('<table style="width:100%;" class="table" ></table>');                         
                                  var row=$('<thead></thead>').append("<tr></tr>")
                                  $('#listadoBusqueda').append(listarP(msg, table, row));
                                  editarPieza();  
                                       
                         }
                         if(msg[0].m==0){
                            $('#Equipo').fadeOut();
                            labelbusqueda.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Equipo no encontrado!</center></div>';
                         }
                                      
                             
                      },
                      error:
                      function (msg) {console.log( msg +" No se pudo realizar la conexion");}
                    });
              
            
          
          }

      });
      $('#Bbuscar').on('click',  function(){

            /*console.log(!$('#tArea2').val());*/
            if(!$('#tArea2').val()==false){
                  $.ajax
                    ({
                      type: "POST",
                      url: "modelo/consultasEquiposyPiezas.php",
                      data: {id:12, data:$('#tArea2').val()},
                      async: false,  
                      dataType: "json",  
                      success:
                      function (msg) 
                      { 
                          console.log(msg[0].m)
                         if(msg[0].m>0){
                                 labelbusqueda.innerHTML=''; 
                                 $('#listadoBusqueda').html("");                        
                                  var table = $('<table style="width:100%;" class="table" ></table>');                         
                                  var row=$('<thead></thead>').append("<tr></tr>")
                                  $('#listadoBusqueda').append(listarE(msg, table, row));
                                  editar();   
                                       
                         }
                         if(msg[0].m==0){
                            $('#Equipo').fadeOut();
                            labelbusqueda.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡Equipo no encontrado!</center></div>';
                         }
                                      
                             
                      },
                      error:
                      function (msg) {console.log( msg +" No se pudo realizar la conexion");}
                    });
              
            
          
          }

      });

     function listarE(msg, table, row){
            row.append($('<th></th>').html(""));
                row.append($('<th></th>').html("<b>Equipo</b>"));                            
                row.append($('<th></th>').html("<b>Marca</b>"));
                row.append($('<th></th>').html("<b>Modelo</b>"));
                row.append($('<th></th>').html("<b>Especificacion</b>"));  
                table.append(row);  
                var row2 = $('<tbody></tbody>');
                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr id="'+msg[i].idequipo+'"></tr>');
                        var fila0=$('<td></td>').text(i);
                        var fila1 = $('<td></td>').text(msg[i].nombretipo);
                        var fila2 = $('<td></td>').text(msg[i].nombremarca);
                        var fila3 = $('<td></td>').text(msg[i].nombremodelo);
                        var fila4 = $('<td></td>').text(msg[i].especificacion);

                        var fila5 = $('<td></td>').append('<a  class="editar btn btn-warning btn-sm"   title="Editar/Modificar" name="'+msg[i].idequipo+'"> <span class="glyphicon glyphicon-pencil"></span></a>');
                        row3.append(fila0);
                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);
                        row3.append(fila5);
                        row2.append(row3);             
                }
                table.append(row2);
                return table;
      }    

        function listarP(msg, table, row){
            row.append($('<th></th>').html(""));
                row.append($('<th></th>').html("<b>Pieza</b>"));                            
                row.append($('<th></th>').html("<b>Marca</b>"));
                row.append($('<th></th>').html("<b>Modelo</b>"));
                row.append($('<th></th>').html("<b>Especificacion</b>"));  
                table.append(row);  
                var row2 = $('<tbody></tbody>');
                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr id="'+msg[i].idpieza+'"></tr>');
                        var fila0=$('<td></td>').text(i);
                        var fila1 = $('<td></td>').text(msg[i].nombretipo);
                        var fila2 = $('<td></td>').text(msg[i].nombremarca);
                        var fila3 = $('<td></td>').text(msg[i].nombremodelo);
                        var fila4 = $('<td></td>').text(msg[i].especificacion);

                        var fila5 = $('<td></td>').append('<a  class="editarPieza btn btn-warning btn-sm"   title="Editar/Modificar" name="'+msg[i].idpieza+'"> <span class="glyphicon glyphicon-pencil"></span></a>');
                        row3.append(fila0);
                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);
                        row3.append(fila5);
                        row2.append(row3);             
                }
                table.append(row2);
                return table;
      }    

/* $('#ttipo').change(function () {
    
        if($('#ttipo').val()==1){
          $('#Equipo').fadeIn();
          $('#Pieza').fadeOut();

        }
        else{
          $('#Equipo').fadeOut();
          $('#Pieza').fadeIn();


        }

});*/


});
 </script>