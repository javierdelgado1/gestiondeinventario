<?php
	session_start();
	
?>
<div class="row">
        <div class="col-xs-12">
             <ol class="breadcrumb">
                <li id="menuprincipal"><a href="#MenuPrincipal">Menu Principal</a>
                </li>
                <li class="active">Configuración del Sistema</li>
            </ol>
        </div>
</div>

<div role="tabpanel">

  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">

	    <li role="presentation" class="active" id="area2"><a href="#area" aria-controls="area" role="tab" data-toggle="tab">Area</a></li>
	    <li role="presentation" id="cargo2"><a href="#cargo" aria-controls="cargo" role="tab" data-toggle="tab">Cargo</a></li>
	    <li role="presentation" id="accion2"><a href="#action" aria-controls="tipo" role="tab" data-toggle="tab">Accion</a></li> 
	    <li role="presentation" id="estado2"><a href="#estado" aria-controls="estado" role="tab" data-toggle="tab">Estado</a></li>
	    <li role="presentation"  id="personal2"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal</a></li>
	    <li role="presentation" id="motivo2"><a href="#motivo" aria-controls="motivo" role="tab" data-toggle="tab">Motivo</a></li>  
	    <li role="presentation" id="proveedor2"><a href="#proveedor" aria-controls="proveedor" role="tab" data-toggle="tab">Proveedor</a></li>
	    <li role="presentation" id="modelo2"><a href="#modelo" aria-controls="modelo" role="tab" data-toggle="tab">Tipo</a></li>
	    <li role="presentation" id="marca2"><a href="#marca" aria-controls="marca" role="tab" data-toggle="tab">Marca</a></li>
	    <li role="presentation" id="tipo2"><a href="#tipo" aria-controls="tipo" role="tab" data-toggle="tab">Modelo</a></li>  
	    <li role="presentation" id="especificaciones2"><a href="#especificaciones" aria-controls="especificaciones" role="tab" data-toggle="tab">Especificaciones</a></li>
	    <!-- <li role="presentation"  id="bdd2"><a href="#bdd" aria-controls="bdd" role="tab" data-toggle="tab">Admin. BDD</a></li> -->
	     <?php  if($_SESSION['tipo']!=2) { ?>
	    <li role="presentation" id="respaldo2"><a href="#respaldo" aria-controls="respaldo" role="tab" data-toggle="tab">Base de Datos</a></li>   
	    <?php } ?>
	  </ul>

	  <div id="espera" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
	  <!-- Tab panes -->
	  <div class="tab-content">
	   <div role="tabpanel" class="tab-pane" id="bdd">
    		

	    </div>
	    <div role="tabpanel" class="tab-pane " id="personal">
			  

	    </div>
	     <!--Tabpanel Area -->
	     <div role="tabpanel" class="tab-pane active" id="area"></div>
	      <!--Tabpanel Area -->

			 <!--Tabpanel Marca -->
	     <div role="tabpanel" class="tab-pane" id="marca"></div>
 			<!--Tabpanel Marca -->

		 <!--Tabpanel Modelo -->
	     <div role="tabpanel" class="tab-pane" id="modelo"></div>
	      <!--Tabpanel Modelo -->
	      <!--Tabpanel Especificaciones -->
	     <div role="tabpanel" class="tab-pane" id="especificaciones"></div>
	      <!--Tabpanel Especificaciones -->
	      <!--Tabpanel estado -->
	     <div role="tabpanel" class="tab-pane" id="estado"></div>
	      <!--Tabpanel estado -->

	     <!--Tabpanel Proveeedor -->
	     <div role="tabpanel" class="tab-pane" id="proveedor">
			 
	     </div>
	      <!--Tabpanel Proveeedor -->
	       <!--Tabpanel Motivo -->
	     <div role="tabpanel" class="tab-pane" id="motivo"></div>
	      <!--Tabpanel Motivo -->
	  <div role="tabpanel" class="tab-pane" id="tipo"></div>
	      <!--Tabpanel Motivo -->
	  	  <div role="tabpanel" class="tab-pane" id="cargo"></div>
	      <!--Tabpanel Motivo -->
 </div>
	  <!-- Tab panes -->


<!--Modal -->








<!--Modal -->




<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>

<!-- <script src="js/bootstrapValidator.min.js"></script> -->
 <script type="text/javascript">
 $(document).ready(function() {

 	$("#personal").hide().load('partials/administrador/configuraciondelsistema/area.html', function(){			
		}).fadeIn(1500);
 	$('#menuprincipal').on('click',  function(){
		/*$("#contenedor").hide().load('partials/menu/menuadministrador.html', function(){			
		}).fadeIn(1500);*/
		window.open('index.php' , '_self');
 	
	});
	$('#respaldo2').on('click',  function(e){
		console.log("click personal");
		e.preventDefault();
       	 $("#espera").css("display", "block");
 		$("#personal").hide().load('partials/administrador/configuraciondelsistema/respaldo.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
	});
	$('#personal2').on('click',  function(e){
		console.log("click personal");
		e.preventDefault();
       	 $("#espera").css("display", "block");
 		$("#personal").hide().load('partials/administrador/configuraciondelsistema/personal.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
	});
	$('#proveedor2').on('click',  function(e){
		console.log("click proveedor");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/proveedor.html', function(){	
        	$("#espera").css("display", "none");					
		}).fadeIn(1500);
		
	});
	$('#area2').on('click',  function(e){
		console.log("click area");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/area.html', function(){	
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#marca2').on('click',  function(e){
		console.log("click marca");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/marca.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#modelo2').on('click',  function(e){
		console.log("click modelo");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/modelo.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#especificaciones2').on('click',  function(e){
		console.log("click especificaciones");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/especificaciones.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#estado2').on('click',  function(e){
		console.log("click estadi");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/estado.html', function(){	
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#motivo2').on('click',  function(e){
		console.log("click motivo");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/motivo.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});
	$('#tipo2').on('click',  function(e){
		console.log("click tipo");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/tipo.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});

	$('#accion2').on('click',  function(e){
		console.log("click accion");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/accion.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});

	$('#cargo2').on('click',  function(e){
		console.log("click accion");
		e.preventDefault();
       	 $("#espera").css("display", "block");


		$("#personal").hide().load('partials/administrador/configuraciondelsistema/cargo.html', function(){
        	$("#espera").css("display", "none");

		}).fadeIn(1500);
		
	});


		

		
     


    
    function limpiarPersonal(){
		tnombre.value="";
		tapellido.value="";
		tidentificacion.value="";
		$('#tcargo').val(0);
		$('.form-group').removeClass('has-success');
		$('.form-group').removeClass('has-error');
    }
    



});
 </script>