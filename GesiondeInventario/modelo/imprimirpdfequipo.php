<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
require('../fpdf.php');
class PDF extends FPDF

{

// Cabecera de página

function Header()

{	

	global $tipo;

	// Logo

	$this->Image('ciplc_logo.png',10,8,33);

	// Arial bold 15

	$this->SetFont('Arial','B',15);

	// Movernos a la derecha

	$this->Cell(80);

	// Título

	$this->Cell(0,10,'VENTA',0,1,'R');

	$this->Cell(0,10,'Id Venta: '.$tipo,0,1,'R');

	// Salto de línea

	$this->Ln(15);

}

}

$pdf = new PDF();
$pdf->Output();



?>